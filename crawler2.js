const puppeteer = require('puppeteer');
var cheerio = require('cheerio');
var request = require('request');
var jwt = require("jsonwebtoken");
var fs = require('fs');
// const util = require('./util');


const UPLOAD_YOUTUBE_LIST_FILE = './upload_log.txt';
const UPLOAD_GIF_LIST_FILE = './upload_gif_log.txt';
const UPLOAD_KEYWORD_LIST_FILE = './upload_keyword.txt';
const UPLOAD_TIMON_LIST_FILE = './upload_timon.log';


// var uploadList = {};
// var uploadGifList = {};
var uploadKeywordList = [];

function readListObjectType(filepath, minLength) {
    var retList = {};
    var readTextAll;
    try {
        readTextAll = fs.readFileSync(filepath).toString();
    } catch(e) {}
    // console.log('readText = ', readTextAll);
    if(readTextAll) {
        readTexts = readTextAll.split('\r\n');
        readTexts.forEach((line) => {
            if(line.length >= minLength) {
                retList[line] = true;
                // console.log('line = ' + line);
            }
        });
    }
    console.log(filepath + ' retList = ', retList); 
    return retList;
}

function writeListObjectType(filepath, writeList) {
    var textAll = '';
    console.log(filepath + ' writeList = ', writeList);
    for(let uploadKey in writeList) {
        textAll = textAll + uploadKey + '\r\n';
    }
    // console.log('textAll = ', textAll);
    fs.writeFileSync(filepath, textAll);
}

// function readUploadList() {
//     uploadList = {};
// }

// function writeUploadList() {
//     var textAll = '';
//     console.log('uploadList = ', uploadList);
//     for(let uploadKey in uploadList) {
//         textAll = textAll + uploadKey + '\r\n';
//     }
//     console.log('textAll = ', textAll);
//     fs.writeFileSync('./upload_log.txt', textAll);
// }

function readUploadKewordList() {
    uploadKeywordList = [];
    var readTextAll;
    try {
        readTextAll = fs.readFileSync('./upload_keyword.txt').toString();
    } catch(e) {}
    console.log('readText = ', readTextAll);
    if(readTextAll) {
        readTexts = readTextAll.split('\r\n');
        readTexts.forEach((line) => {
            if(line.length > 0) {
                uploadKeywordList.push(line);
            }
        });
    }
    console.log('uploadKeywordList = ', uploadKeywordList);   
}


function writeUploadKewordList() {
    var textAll = '';
    console.log('uploadKeywordList = ', uploadKeywordList);
    for(let i = 0; i < uploadKeywordList.length; i++) {
        textAll = textAll + uploadKeywordList[i] + '\r\n';
    }
    console.log('textAll = ', textAll);
    fs.writeFileSync('./upload_keyword.txt', textAll);
}


// function readUploadGifList() {
//     uploadGifList = {};
//     var readTextAll;
//     try {
//         readTextAll = fs.readFileSync('./upload_gif_log.txt').toString();
//     } catch(e) {}
//     console.log('readText = ', readTextAll);
//     if(readTextAll) {
//         readTexts = readTextAll.split('\r\n');
//         readTexts.forEach((line) => {
//             if(line.length > 3) {
//                 uploadGifList[line] = true;
//                 console.log('line = ' + line);
//             }
//         });
//     }
//     console.log('uploadList = ', uploadList);    
// }

// function writeUploadGifList() {
//     var textAll = '';
//     console.log('uploadGifList = ', uploadGifList);
//     for(let uploadKey in uploadGifList) {
//         textAll = textAll + uploadKey + '\r\n';
//     }
//     console.log('textAll = ', textAll);
//     fs.writeFileSync('./upload_gif_log.txt', textAll);
// }


const createToken = (userDoc, secret) => {
    let tokenData = {
        _id: userDoc._id,
        name: userDoc.name,
        email: userDoc.email,
        memberGrade: userDoc.memberGrade,
        auth_type: userDoc.auth_type,
        isActivated: userDoc.isActivated
    };

    return new Promise((resolve, reject) => {
        jwt.sign(
            tokenData,
            secret,
            {
                expiresIn: "999d",
                issuer: "weneepl.com",
                subject: "userInfo"
            },
            (err, token) => {
                if (err) reject(err);
                resolve(token);
            }
        );
    });
};

var gifAppId = 18378;
var naverAppId = 32009;
var timonAppId = 32045;
var naverAccessToken;
createToken({
    _id:64051
}, 'weneepl_secret').then((token) => {
    naverAccessToken = token;
    console.log('naverAccessToken = ', naverAccessToken);
});
var timonAccessToken;
createToken({
    _id:64431
}, 'weneepl_secret').then((token) => {
    timonAccessToken = token;
    console.log('timonAccessToken = ', timonAccessToken);
});
var gifAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjEsIm5hbWUiOiLsiqTti7DruIwiLCJlbWFpbCI6Im1hZ21hOThAZ21haWwuY29tIiwibWVtYmVyR3JhZGUiOjEsImF1dGhfdHlwZSI6MSwiaXNBY3RpdmF0ZWQiOnRydWUsImlhdCI6MTU0ODY0NzQwNywiZXhwIjoxNjM0OTYxMDA3LCJpc3MiOiJ3ZW5lZXBsLmNvbSIsInN1YiI6InVzZXJJbmZvIn0.FBl6DxWNRiSjJmYdiXzUsyIIDQ67WTLnTMZJZVhi1tc'



function uploadImageURL(url, appiId, accessToken) {
    var jsonData = {
        userId: 1,
        appId: appiId,
        urls: [url]
    };
    // console.log('jsonData = ', jsonData);
    return new Promise((resolve, reject) => {
        request(
            {
                headers: {
                    'content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'x-access-token',
                    'x-access-token': accessToken
                },
                // url: 'https://api-service-v4.weneepl.com/api/v4.0/images/uploadUrl',
                url: 'http://192.168.0.6:8085/api/v4.0/images/uploadUrl',
                method: 'POST',
                body: JSON.stringify(jsonData)
            }, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    console.log('result = ', bodyObj['result']);
                    if(bodyObj['result']) {
                        resolve(bodyObj['links'][0]);
                    } else {
                        reject(body);
                    }
                }
            }
        );
    });
}


function uploadImageFile(filepath, appiId, accessToken) {
    var formData = {
        userId: 1,
        appId: appiId,
        file: fs.createReadStream(filepath),
    };
    // console.log('jsonData = ', jsonData);
    return new Promise((resolve, reject) => {
        request(
            {
                headers: {
                    'content-type': 'multipart/form-data',
                    'Access-Control-Allow-Headers': 'x-access-token',
                    'x-access-token': accessToken
                },
                // url: 'https://api-service-v4.weneepl.com/api/v4.0/images/uploadUrl',
                url: 'http://192.168.0.6:8085/api/v4.0/images',
                method: 'POST',
                formData: formData
            }, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    console.log('result = ', bodyObj['result']);
                    if(bodyObj['result']) {
                        resolve(bodyObj['link']);
                    } else {
                        reject(body);
                    }
                }
            }
        );
    });
}

function writeYoutubeArticle(title, videoUrl, imageUrl) {
    let content = '<p><span class="note-span-monopoly note-video-wrapper" contenteditable="false"><iframe allowfullscreen="" frameborder="0" src="' + videoUrl + '" width="640" height="360" class="note-video-clip" style="max-width: 100%; width: 90%;"></iframe></span><br></span></p>'
    //이시간 이슈영상
    return writeArticle(naverAppId, '5c545d547e52366673ec3354', naverAccessToken, title, '', content, [imageUrl], [], [], 0);
}

function writeGifArticle(title, oriUrl, imageUrl) {
    let content = '<p><a href="' + oriUrl + '"><img class="note-img" style="max-width: 100%; width: 90%; height: auto;" src="' + imageUrl + '"></a></p><p><br></p><p><a href="' + oriUrl + '">원문 보기</a></p>';
    //블랙박스영상
    return writeArticle(gifAppId, '5bef7861147c7019a99d4c35', gifAccessToken, title, '', content, [imageUrl], [], [], 0);
    //테스트앱
    // return writeArticle(1, '5bdff983d3d31a184092fff7', 1, title, '', content, [imageUrl], [], [], 0);
}

function writeTimonArticle(productItem, imageUrls) {
    let content = '<p>' + productItem.titleName + '<br><b>' + productItem.price + '원</b><br><p>';
    for (let i = 0; i < imageUrls.length; i++) {
        content = content + '<p><a href="' + productItem.url + '"><img class="note-img" style="max-width: 100%; width: 90%; height: auto;" src="' + imageUrls[i] + '"></a></p>'
    }
    content = content + '<p><br></p><p><a href="' + productItem.url + '">쇼핑하러 가기</a></p>';
    return writeArticle(timonAppId, '5c5eef5c9a270b58980cf7c7', timonAccessToken, productItem.titleName, '', content, imageUrls, [], [], 0);
}

function writeArticle(appId, menuId, accessToken, title, summary, content, images, videos, files, size) {

    let jsonData = {
        menuId: menuId,
        appId: appId,
        title: title,
        content: content,
        summary: summary,
        images: images,
        videos: videos,
        files: files,
        size: size
    }

    return new Promise((resolve, reject) => {
        request(
            {
                headers: {
                    'content-type': 'application/json',
                    'Access-Control-Allow-Headers': 'x-access-token',
                    'x-access-token': accessToken
                },
                // url: 'http://192.168.0.6:8085/api/v4.0/boards/' + menuId + '/write',
                url: 'https://api-service-v4.weneepl.com/api/v4.0/boards/' + menuId + '/write',
                method: 'POST',
                body: JSON.stringify(jsonData)
            }, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    console.log(body);
                    var bodyObj = JSON.parse(body);
                    console.log('result = ', bodyObj['result']);
                    if(bodyObj['result']) {
                        resolve(bodyObj['article']);
                    } else {
                        reject(body);
                    }
                }
            }
        );
    });


}
// uploadImageURL('https://i.ytimg.com/vi/JNIw6k2e85Y/hqdefault.jpg');
// writeYoutubeArticle('유튜브로봇데스트', '//www.youtube.com/embed/Y98q3CDXJ98', 'https://storage.weneepl.com/v4/5c53a67352f2b46cf07df455');

var isWorking = true;
var timeIntervalSec = 10*60;

var naverNextExeTime = 3*60*60;
var naverTimeIntervalSec = 3*60*60;
var gifNextExeTime = 12*60*60;
var gifTimeIntervalSec = 18*60*60;
var timonNextExeTime = 0.5*60*60;
var timonTimeIntervalSec = 3*60*60;
var curTime = 0;

var searchKeyword = '블랙박스 영상 gif'
// getSubjectByNaver();
// getGifByGoogle();
// getBestDealTimon();
// uploadImageFile('./yoursite.png', timonAppId).then((result) => {
//     console.log('result : ', result);
// });
loopRobotWork(timeIntervalSec);

async function getBestDealTimon() {
    var dbWriteCompleted = false;
    var uploadTimonList = readListObjectType(UPLOAD_TIMON_LIST_FILE, 1);
    // console.log('uploadTimonList = ', uploadTimonList);
    // var browser = await puppeteer.launch({headless: false, args : [
    var browser = await puppeteer.launch({args : [
        '--window-size=1280,960'
    ]});
    var page = await browser.newPage();
    await page.setViewport({
        width: 1120,
        height: 800
    });
    var urls = [
        'http://www.ticketmonster.co.kr/best',
        'http://www.wemakeprice.com/best'
        // 'http://www.ticketmonster.co.kr/planning/PLAN_DCXfbM9PDF', //단 하루
        // 'http://www.ticketmonster.co.kr/planning/PLAN_dSluwFwqI8', //오늘의 여기
        // 'http://www.ticketmonster.co.kr/planning/PLAN_OksYSJZlLU', //오늘의 여행
        // 'http://www.ticketmonster.co.kr/planning/PLAN_HOFcYwHa1E', //1212타임
        // 'http://www.ticketmonster.co.kr/planning/PLAN_z0zWieDJby', //티몬 균일가
        // 'http://www.ticketmonster.co.kr/planning/PLAN_xENFfoSUT9', //99 균일가
        // 'http://www.ticketmonster.co.kr/planning/PLAN_YrlFRLa7TY', //49 균일가
        // 'http://www.ticketmonster.co.kr/planning/PLAN_jTmBX8ujEe', //패션 균일가
    ]
    try {
        var totalDealList = [];
        for(let urlIdx = 0; urlIdx < urls.length; urlIdx++) {
            await page.goto(urls[urlIdx]);
            await page.waitFor(2000);
            let dealList = await page.evaluate((category) => {
                var dealList = [];
                console.log('page.evaluate!!');
                if(category.indexOf('ticketmonster') >= 0) {
                    var mainDiv = document.querySelector('div.lst_deal');
                    // console.log('mainDiv = ', mainDiv);
                    if(!mainDiv) {
                        mainDiv = document.querySelector('div.category_deallist');
                    }
                    var listWrap = mainDiv.querySelector('div.list_wrap');
                    // console.log('listWrap = ', listWrap);
                    var liList = listWrap.querySelectorAll('li.item');
                    // console.log('liList = ', liList);
                    for (let i = 0; i < liList.length; i++) {
                        var ahref = liList[i].querySelector('a.anchor');
                        // console.log('ahref = ', ahref);
                        var url = ahref.getAttribute('href');
                        var titleName = '[티몬]' + ahref.querySelector('.title_name').textContent;
                        // console.log('titleName = ', titleName);
                        var buyCountNode = ahref.querySelector('.buy_count');
                        if(buyCountNode) {
                            var buyCount = parseInt(buyCountNode.querySelector('i').textContent.replace(',', ''));
                            // console.log('buyCount = ', buyCount);
                            if(buyCount > 10) {
                                let originalPrice;
                                let curPrice;
                                let originalPriceNode = ahref.querySelector('.original_price');
                                if (originalPriceNode) {
                                    originalPrice = parseInt(originalPriceNode.querySelector('i').textContent.replace(',', ''));
                                }
                                let priceNodes = ahref.querySelectorAll('.price');
                                if (priceNodes && priceNodes.length > 0) {
                                    var priceINode = priceNodes[priceNodes.length-1].querySelector('i');
                                    if (priceINode) {
                                        curPrice = parseInt(priceINode.textContent.replace(',', ''));
                                    }
                                }
                                let productInfo = {
                                    category: category,
                                    url: url,
                                    buyCount: buyCount,
                                    titleName: titleName,
                                    originalPrice: originalPrice,
                                    price: curPrice
                                };
                                dealList.push(productInfo);
                            }
                        }
                    }
                } else if(category.indexOf('wemakeprice') >= 0) {
                    // console.log('wemakeprice');
                    var listWrap = document.querySelector('ul#wrap_main_best');
                    // console.log('listWrap = ', listWrap);
                    var liList = listWrap.querySelectorAll('li');
                    // console.log('liList = ', liList);
                    for (let i = 0; i < liList.length; i++) {
                        var ahref = liList[i].querySelector('a');
                        // console.log('ahref = ', ahref);
                        var url = ahref.getAttribute('href');
                        if(url.indexOf('http') < 0) {
                            url = 'http://www.wemakeprice.com' + url;
                        }
                        var titleName = '[위메프]' + ahref.querySelector('.tit_desc').textContent;

                        // console.log('ahref.parentNode = ', ahref.parentNode);
                        var cardOptionNode = ahref.parentNode.querySelector('.card_option');
                        // console.log('cardOptionNode = ', cardOptionNode);
                        var buyCountNode = cardOptionNode.querySelector('.txt_num');
                        // console.log('buyCountNode = ', buyCountNode);
                        if(buyCountNode) {
                            var buyCount = parseInt(buyCountNode.querySelector('strong.point').textContent.replace(',', ''));
                            // console.log('buyCount = ', buyCount);
                            if(buyCount > 10) {
                                let priceInfoNode = ahref.querySelector('.box_desc').querySelector('.txt_info');
                                // console.log('priceInfoNode = ', priceInfoNode);
                                let originalPrice;
                                let curPrice;
                                let originalPriceNode = priceInfoNode.querySelector('.prime');
                                if (originalPriceNode) {
                                    originalPrice = parseInt(originalPriceNode.textContent.replace(',', ''));
                                }
                                let priceNode = priceInfoNode.querySelector('.sale');
                                if (priceNode) {
                                    curPrice = parseInt(priceNode.textContent.replace(',', ''));
                                }
                                let productInfo = {
                                    category: category,
                                    url: url,
                                    buyCount: buyCount,
                                    titleName: titleName,
                                    originalPrice: originalPrice,
                                    price: curPrice
                                };
                                dealList.push(productInfo);
                            }
                        }
                    }
                }
                return Promise.resolve(dealList);
            }, urls[urlIdx]);
            console.log('category : ' + urls[urlIdx]);
            if(urls[urlIdx].indexOf('/best') < 0) { //베스트 카테고리 제외하고 판매수량 기준 정렬
                console.log('Sorting');
                dealList.sort(dealListSort);
            }
            totalDealList.push(dealList);
        }
        // console.log('totalDealList = ', totalDealList);

        var pickObject;
        for(let dealIdx = 0; dealIdx < 10 && !pickObject; dealIdx++) {
            for(let listIdx = 0; listIdx < totalDealList.length && !pickObject; listIdx++) {
                if(totalDealList[listIdx][dealIdx] && !uploadTimonList[totalDealList[listIdx][dealIdx].url]) {
                    pickObject = totalDealList[listIdx][dealIdx];
                }
            }
        }
        console.log('pickObject = ', pickObject);
        await page.setViewport({
            width: 1120,
            height: 800
        });
        await page.goto(pickObject.url);
        await page.waitFor(2000);
        await page.evaluate(() => {
            window.scrollBy(0, 200);
            document.querySelector('html').style.overflow = 'hidden';
        });
        var dealScreenShotFile = 'deal_screenshot.png';
        await page.screenshot({
            path: dealScreenShotFile
        });
        var dealImages = await page.evaluate((dealUrl) => {
            console.log('page.evaluate!!');
            var dealImages = [];
            if(dealUrl.indexOf('ticketmonster') >= 0) {
                // var mainImage = document.querySelector('#_mainDealImage').querySelector('img');
                // // console.log('mainImage = ', mainImage);
                // dealImages.push(mainImage.getAttribute('src'));
                var detailImages = document.querySelector('#_wrapDealContents').querySelectorAll('img');
                // console.log('detailImages = ', detailImages);
                for (let i = 0; i < detailImages.length && dealImages.length < 5; i++) {
                    console.log('1.detailImages = ', detailImages[i].dataset.src);
                    console.log('2.detailImages = ', detailImages[i].getAttribute('src'));
                    dealImages.push(detailImages[i].dataset.src);
                }    
            } else if(dealUrl.indexOf('wemakeprice') >= 0) {
                // var mainImage = document.querySelector('.info_img').querySelector('img');
                // // console.log('mainImage = ', mainImage);
                // dealImages.push(mainImage.getAttribute('src'));
                var imageDiv = document.querySelector('.detail_img');
                if(!imageDiv) {
                    imageDiv = document.querySelector('.prd_intro_img');
                }
                if(!imageDiv) {
                    imageDiv = document.querySelector('.option-img-selecter');
                }
                if(!imageDiv) {
                    imageDiv = document.querySelector('.deal_detailimg');
                }
                if(imageDiv) {
                    var detailImages = imageDiv.querySelectorAll('img');
                    // console.log('detailImages = ', detailImages);
                    for (let i = 0; i < detailImages.length && dealImages.length < 5; i++) {
                        console.log('2.detailImages = ', detailImages[i].getAttribute('src'));
                        dealImages.push(detailImages[i].getAttribute('src'));
                    }    
                }
            }
            return Promise.resolve(dealImages);
        }, pickObject.url);
        console.log('dealImages: ', dealImages);

        var newDealImages = [];

        let mainImageUrl = await uploadImageFile('./' + dealScreenShotFile, timonAppId, timonAccessToken);
        newDealImages.push(mainImageUrl);
        for (let i = 0; i < dealImages.length; i++) {
            if(dealImages[i]) {
                let imageUrl = await uploadImageURL(dealImages[i], timonAppId, timonAccessToken);
                console.log('imageUrl = ', imageUrl);
                newDealImages.push(imageUrl);
            }
        }
        let article = writeTimonArticle(pickObject, newDealImages);
        console.log('article = ', article);
        uploadTimonList[pickObject.url] = true;
        writeListObjectType(UPLOAD_TIMON_LIST_FILE, uploadTimonList);
        dbWriteCompleted = true;
    } catch(e) {
        console.log(e);
    }
    await browser.close();
    return dbWriteCompleted;
}

function dealListSort(a, b) {
    if(a.buyCount > b.buyCount) return -1;
    if(a.buyCount < b.buyCount) return 1;
    return 0;
}


async function getGifByGoogle() {
    var dbWriteCompleted = false;
    var uploadGifList = readListObjectType(UPLOAD_GIF_LIST_FILE, 3);
    var browser = await puppeteer.launch({args : [
        '--window-size=1280,960'
    ]});
    var page = await browser.newPage();
    await page.setViewport({
        width: 1280,
        height: 960
    });
    var url = 'https://www.google.com/search?q=' + encodeURI(searchKeyword) + '&newwindow=1&source=lnms&tbm=isch'
    // url = 'https://datalab.naver.com/keyword/realtimeList.naver?period=3h&where=main';
    try {
        await page.goto(url);
        console.log('completed!!');
        var gifUrls = await page.evaluate(() => {
            console.log('page.evaluate!!');
            var gifUrls = [];
            var imgs = document.querySelectorAll('img.rg_ic');
            console.log('imgs = ', imgs);
            for (let i = 0; i < imgs.length; i++) {
                var aNode = imgs[i].parentNode;
                console.log('aNode = ', aNode);
                var ahref = aNode.getAttribute('href');
                console.log('ahref = ', ahref);
                gifUrls.push('https://www.google.com' + ahref);
            }
            return Promise.resolve(gifUrls);
        });
        console.log("!!!!!!!!!!!!!!!!!!!!!");
        // page.close();
        let curUploadGifList = [];
        const uploadLimit = 1;
        for(let i = 0; i < gifUrls.length && curUploadGifList.length < uploadLimit; i++) {
            // console.log('1.gifUrls[i] = ', gifUrls[i]);
            await page.goto(gifUrls[i]);
            await page.waitFor(2000);

            // var anchors = await $$(page, 'a.rg_l');
            // for (let j = 0; j < anchors.length && curUploadGifList.length < uploadLimit; j++) {
            //     anchors[i].click();
            //     await page.waitFor(1000);

                var retVal = await page.evaluate(() => {
                    let retVal = {};
                    var imgs = document.querySelectorAll('.irc_mi');
                    console.log('imgs = ', imgs);
                    for (let i = 0; i < imgs.length; i++) {
                        // let img = aTags[i].childNodes[0];
                        let imgSrc = imgs[i].getAttribute('src');
                        console.log('img[' + i + '] src = ', imgSrc);
                        if (imgSrc && imgSrc.length > 3) {
                            retVal.imgSrc = imgSrc;
                        }
                    }
                    var titles = document.querySelectorAll('.irc_pt');
                    console.log('titles = ', titles);
                    for (let i = 0; i < titles.length; i++) {
                        let titleText = titles[i].textContent;
                        let oriUrl = titles[i].getAttribute('href');
                        console.log('titles[' + i + '] = ', titles[i].textContent);
                        if (titleText && titleText.length > 0) {
                            retVal.titleText = titleText;
                            retVal.oriUrl = oriUrl;
                        }
                    }
                    return Promise.resolve(retVal);
                });
                if (retVal.imgSrc && retVal.titleText) {
                    if (!uploadGifList[retVal.imgSrc]) {
                        curUploadGifList.push(retVal);
                    }
                }
            // }
        }

        for (let i = 0; i < curUploadGifList.length; i++) {
            console.log('curUploadGifList[' + i + '] = ', curUploadGifList[i]);
            let imageUrl = await uploadImageURL(curUploadGifList[i].imgSrc, gifAppId, gifAccessToken);
            console.log('imageUrl = ', imageUrl);
            let article = writeGifArticle(curUploadGifList[i].titleText, curUploadGifList[i].oriUrl, imageUrl);
            uploadGifList[curUploadGifList[i].imgSrc] = true;
            console.log('article = ', article);
        }
        writeListObjectType(UPLOAD_GIF_LIST_FILE, uploadGifList);
        dbWriteCompleted = true;
    } catch(e) {
        console.log(e);
    }
    await browser.close();
    return dbWriteCompleted;
}

async function $$ (page, selector) {
    const ids = await page.evaluate((selector) => {
      const list = document.querySelectorAll(selector)
      const ids = []
      for (const element of list) {
        const id = selector + ids.length;
        let href = element.getAttribute('href');
        // if(href.indexOf('/imgres') >= 0) {
            console.log('href = ', href);
            ids.push(id)
            element.adAttribute('class', id)
        // }
      }
      return ids
    }, selector)
    const getElements = []
    for (const id of ids) {
      getElements.push(page.$(`${selector}[puppeteer=${id}]`))
    }
    return Promise.all(getElements)
}



function myTimeout(sec) {
    // console.log('0.setTimeout completed');
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('1.setTimeout completed');
            resolve();
        }, sec);
    });
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function loopRobotWork(intervalSec) {
    console.log('Start loopRobotWork interval = ' + intervalSec + ' Seconds');
    // await writeArticle(32009, '5c545d547e52366673ec3354', 1, '테스트', '테스트', '<p>테스트<br></p>', [], [], [], 0)
    while(isWorking) {
        console.log('curTime = ' + curTime + ' Seconds');
        if (naverNextExeTime <= curTime) {
            let result = await getSubjectByNaver();
            if(result) {
                naverNextExeTime = curTime + getRandomInt(parseInt(naverTimeIntervalSec/2), parseInt(naverTimeIntervalSec+naverTimeIntervalSec/2));
            }
            console.log('naverNextExeTime = ' + naverNextExeTime + ' Seconds(curTime = ' + curTime + ')');
        }
        if (gifNextExeTime <= curTime) {
            let result = await getGifByGoogle();
            if(result) {
                gifNextExeTime = curTime + getRandomInt(parseInt(gifTimeIntervalSec/2), parseInt(gifTimeIntervalSec+gifTimeIntervalSec/2));
            }
            console.log('gifNextExeTime = ' + gifNextExeTime + ' Seconds(curTime = ' + curTime + ')');
        }        
        if (timonNextExeTime <= curTime) {
            let result = await getBestDealTimon();
            // timonNextExeTime = curTime + getRandomInt(parseInt(timonTimeIntervalSec*9/10), parseInt(timonTimeIntervalSec+timonTimeIntervalSec*11/10));
            if(result) {
                timonNextExeTime = curTime + timonTimeIntervalSec;
            }
            console.log('timonNextExeTime = ' + timonNextExeTime + ' Seconds(curTime = ' + curTime + ')');
        }        
        let curDate = new Date();
        let curHour = curDate.getHours();
        console.log('curHour = ' + curHour + ' H');
        await myTimeout(intervalSec*1000);
        // if(curHour < 1 || curHour >= 7 ) {
            curTime += intervalSec;
        // }
        // await getGifByGoogle();
        // await myTimeout(intervalSec*1000);
    }
}


function requestUrl(url) {
    return new Promise((resolve, reject) => {
        request(url, function(err, res, html){
            if (err) {
                reject(err);
            } else {
                resolve(html);
            }
        });
    });
}


function getJSONString(htmlData, JSONKey) {
    var startIndex = htmlData.indexOf(JSONKey);
    if (startIndex < 0) return null;
    startIndex = htmlData.indexOf('{', startIndex);
    if (startIndex < 0) return null;
    var startCount = 1;
    var curIndex = startIndex + 1;
    var limitLoopCnt = 1000000;
    while (startCount > 0 && --limitLoopCnt > 0) {
      let bracketStart = htmlData.indexOf('{', curIndex);
      let bracketEnd = htmlData.indexOf('}', curIndex);
      if (bracketStart < 0) {
        bracketStart = Number.MAX_VALUE;
      }
      if (bracketEnd < 0) break;
      if (bracketStart < bracketEnd) {
        startCount++;
        curIndex = bracketStart + 1;
      } else {
        startCount--;
        curIndex = bracketEnd + 1;
      }
    }
    var retString = htmlData.substring(startIndex, curIndex);
    // console.log('retString = ', retString);
    return retString;
}


function requestGoogleImageSearch(keyword) {
    var newKeyword = keyword.replace(' ', '+');
    var url = 'https://www.google.com/search?q=' + encodeURI(newKeyword) + '&newwindow=1&source=lnms&tbm=isch'
    console.log('url = ', url);
    return new Promise((resolve, reject) => {
        requestUrl(url).then((html) => {
            console.log('html = ', html);
            resolve();
        });
    });
}


function requestYoutubeSearch(keyword, prevRank, prevScore) {
    var newKeyword = keyword.replace(' ', '+');
    console.log('newKeyWord = ', newKeyword);
    var url = 'https://www.youtube.com/results?search_query=' + encodeURI(newKeyword);
    return new Promise((resolve, reject) => {
        requestUrl(url).then((html) => {
            // console.log('html = ', html);
            // var jsonStr = getJSONString(html, 'window["ytInitialData"] = ');
            // console.log('jsonStr = ', jsonStr);
            var $ = cheerio.load(html);
            var lockupContents = $('.yt-lockup-content');
            // console.log('lockupContents.length = ', lockupContents.length);
            let count = 0;
            var resultArray = []; 
            lockupContents.each(function(i, lockupContent) {
                let aTag = $(this).children().children();
                // console.log('aTag = ', aTag);
                let metaTag = $(this).children('.yt-lockup-meta').children().children().first();
                // console.log('metaTag = ', metaTag);
                let ahref = aTag.attr('href');
                if(ahref && count < 20) {
                    // console.log('ahref = ', ahref);
                    let metaText = metaTag.text();
                    // console.log('metaText = ', metaText);
                    let titleText= aTag.attr('title');
                    // console.log('titleText = ', titleText);
                    let videoId;
                    let videoIdIdx = ahref.indexOf('?v=');
                    if(videoIdIdx > 0) {
                        videoId = ahref.substring(videoIdIdx + 3);
                    }
                    // console.log('videoId = ', videoId);
                    let timeScore = 0;
                    if(metaText.indexOf('분 전') > 0 || metaText.indexOf('초 전') > 0) {
                        timeScore = 75;
                    } else {
                        let hourIdx = metaText.indexOf('시간');
                        let dayIdx = metaText.indexOf('일 전');
                        let monthIdx = metaText.indexOf('개월 전');
                        if(hourIdx > 0) {
                            timeScore = Math.max(50, 50 + (24 - parseInt(metaText.substring(0, hourIdx))));
                        } else if(dayIdx > 0) {
                            timeScore = Math.max(0, 0 + (24 - parseInt(metaText.substring(0, dayIdx)))*2);
                        } else if(monthIdx > 0) {
                            timeScore = 0 - parseInt(metaText.substring(0, monthIdx))*3;
                        } else {
                            timeScore = -100;
                        }
                    }
                    let youtubeScore = (20 - (count+1)) * 3;
                    resultArray.push({
                        keyword: keyword,
                        title: titleText,
                        videoId: videoId,
                        prevRank: prevRank,
                        prevScore: prevScore,
                        youtubeScore: youtubeScore,
                        youtubeRank: count+1,
                        timeScore: timeScore,
                        timeText: metaText
                    });
                }
                count++;
            });
            // for (let i = 0; i < lockupContent.length; i++) {
            //     // let ahref = lockupContent.find('.yt-uix-tile-link');
            //     console.log('ahref = ', lockupContent[i].attr('href'));
            // }
            // console.log('lockupContent = ', lockupContent);
            resolve(resultArray);
        }).catch((err) => {
            reject(err);
        });
    });
}

async function getSubjectByNaver() {
    var dbWriteCompleted = false;
    var uploadYoutubeList = readListObjectType(UPLOAD_YOUTUBE_LIST_FILE, 3);
    readUploadKewordList();
    // var browser = await puppeteer.launch({headless: false, args : [
    var browser = await puppeteer.launch({args : [
            '--window-size=1280,960'
    ]});
    const page = await browser.newPage();
    page.on('console', msg => {
        for (let i = 0; i < msg.args.length; ++i)
          console.log(`${i}: ${msg.args[i]}`);
      });
      await page.setViewport({
        width: 1280,
        height: 960
    })
    try {
        await page.goto('https://datalab.naver.com/keyword/realtimeList.naver?period=now&where=main');

        var ranksTitle = await page.evaluate(() => {
            var ranksTitle = [];
            var mainDiv = document.querySelector('.section_lst_area.carousel_area');
            // console.log('mainDiv = ', mainDiv);
            var rankDiv = mainDiv.querySelector('.keyword_rank');
            // console.log('rankDiv = ', rankDiv);
            var rankUl = rankDiv.querySelector('ul.rank_list');
            // console.log('rankUl = ', rankUl);
            var ranks = rankUl.querySelectorAll('span.title');
            // console.log('ranks = ', ranks);
            for (let i = 0; i < ranks.length; i++) {
                console.log('ranks[' + i + '] = ', ranks[i].textContent);
                ranksTitle.push(ranks[i].textContent);
            }
            return Promise.resolve(ranksTitle);
        });

        var totalList = [];
        for (let i = 0; i < ranksTitle.length && i < 10; i++) {
            let curList = await requestYoutubeSearch(ranksTitle[i], i + 1, Math.max(0, (20-i-1)*5));
            totalList = totalList.concat(curList);
        }
        for (let i = 0; i < totalList.length; i++) {
            let curKeyword = totalList[i].keyword;
            let curTitle = totalList[i].title;
            let relationWord = '';
            let relationScore = 0;
            for (let j = 0; j < ranksTitle.length && relationScore == 0; j++) {
                if(curKeyword != ranksTitle[j]) {
                    if(ranksTitle[j].indexOf(curKeyword) < 0 && curKeyword.indexOf(ranksTitle[j]) < 0 && curTitle.indexOf(ranksTitle[j]) >= 0) {
                        relationScore = Math.max(0, (20-j-1)*3);
                        relationWord = ranksTitle[j];
                    }
                }
            }
            totalList[i].relationWord = relationWord;
            totalList[i].relationScore = relationScore;

            let duplicateScore = 0;
            for(let j = 0; j < uploadKeywordList.length ; j++) {
                if (curKeyword == uploadKeywordList[j]) {
                    // console.log('1.curKeyword : %s, duplicateScore : %i', curKeyword, duplicateScore);
                    duplicateScore = duplicateScore - Math.max(0, j - (uploadKeywordList.length - 6))*10;
                    // console.log('2.curKeyword : %s, duplicateScore : %i', curKeyword, duplicateScore);
                }
            }
            totalList[i].duplicateScore = duplicateScore;
            // console.log('curKeyword : %s, duplicateScore : %i', curKeyword, duplicateScore);
        }
        totalList.sort(scoreCompare);
        // console.log('totalList = ', totalList);

        var pickItem;
        for (let i = 0; i < totalList.length && !pickItem; i++) {
            if (!uploadYoutubeList[totalList[i].videoId]) {
                pickItem = totalList[i];
            }
        }

        console.log('pickItem = ', pickItem);

        if (pickItem) {
            let thumbImgUrl = 'https://i.ytimg.com/vi/' + pickItem.videoId + '/hqdefault.jpg';
            // console.log('thumbImgUrl = ', thumbImgUrl);
    
            let imageUrl = await uploadImageURL(thumbImgUrl, naverAppId, naverAccessToken).then();
            // console.log('imageUrl = ', imageUrl);
            let videoUrl = '//www.youtube.com/embed/' + pickItem.videoId;
            let article = writeYoutubeArticle(pickItem.title, videoUrl, imageUrl);
            uploadKeywordList.push(pickItem.keyword);
            uploadYoutubeList[pickItem.videoId] = true;
            writeListObjectType(UPLOAD_YOUTUBE_LIST_FILE, uploadYoutubeList);
            writeUploadKewordList();
            console.log('article = ', article);
            dbWriteCompleted = true;
        }
    } catch(e) {
        console.log(e);
    }
    await browser.close();
    return dbWriteCompleted;
}

function scoreCompare(a, b) {
    var aTotal = a.prevScore + a.timeScore + a.youtubeScore + a.relationScore + a.duplicateScore;
    var bTotal = b.prevScore + b.timeScore + b.youtubeScore + b.relationScore + b.duplicateScore;
    if(aTotal > bTotal) return -1;
    if(aTotal < bTotal) return 1;
    return 0;
}

