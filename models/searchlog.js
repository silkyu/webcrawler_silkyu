var mongoose = require("mongoose");
const config = require("../config.js");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);

var connection = mongoose.connection;

var searchlogSchema = mongoose.Schema({
    userKey: String,
    adLoaded: Number,
    departureId : String,
    arrivalId : String,
    isKorean: Number,
    isFlyRound: Number,
    minNight: Number,
    maxNight: Number,
    searchStart: String,
    searchEnd: String,
    departDay: [String],
    arriveDay: [String],
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model("SearchLog", searchlogSchema);
