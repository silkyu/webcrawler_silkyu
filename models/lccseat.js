var mongoose = require("mongoose");
const config = require("../config.js");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);

var connection = mongoose.connection;

var lccseatSchema = mongoose.Schema({
    airlineName : String,
    departureName : String,
    departureId : String,
    arrivalName : String,
    arrivalId : String,
    seatState : [{sdate : String, stime : String, aircraftType : String, flightTime : String, seatData:[{seatType: String, seatNum: Number, fare: Number, tax: Number, totalFare: Number, condition: mongoose.Schema.Types.Mixed}]}],
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model("LccSeat", lccseatSchema);
