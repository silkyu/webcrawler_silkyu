var mongoose = require("mongoose");

var menuSchema = mongoose.Schema({
    appId: Number,
    title: String,
    nickname: String,
    component: String,
    versionCode: {type:Number, default:1},
    icon: String,
    navigators: [String],
    permissions: {
        read: [],
        write: [],
        show: [],
        comment: []
    },
    templateOptions: [mongoose.Schema.Types.Mixed],
    type: String,
    menuId: String,
    parentId: mongoose.Schema.Types.ObjectId,
    isModal: { type: Boolean, default: false },
    children: { type: mongoose.Schema.Types.Mixed, default: {} },
    deleted: {type: Boolean, default: false}
});

menuSchema.statics.get = function(menuId) {
    return new Promise((resolve, reject) => {
        this.findById(menuId, (err, menu) => {
            if (err != undefined) {
                console.log('err - ', err);
            }
            if (menu == undefined) {
                reject(null);
            } else {
                resolve(menu);
            }
        });
    });
};

module.exports = mongoose.model("Menu", menuSchema);