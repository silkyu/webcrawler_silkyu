var mongoose = require("mongoose");
const config = require("../config.js");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);

var connection = mongoose.connection;

var asianaseatSchema = mongoose.Schema({
    departureName : String,
    departureId : String,
    arrivalName : String,
    arrivalId : String,
    seatState : [{sdate : String, stime : String, mileage : [Number], tax : [Number], aircraftType : String, flightTime : String, seatNum:[Number]}],
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model("AsianaSeat", asianaseatSchema);
