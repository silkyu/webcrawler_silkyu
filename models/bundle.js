var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");
// var Promise = require("bluebird");
// var mongoosePaginate = require("mongoose-paginate");
// var async = require("async");
var mexp = require("mongoose-elasticsearch-xp");

const User = require("./user");
const App = require("./app");

const config = require("../config.js");
// const util = require("../util");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);
var connection = mongoose.connection;
autoIncrement.initialize(connection);

var bundleSchema = mongoose.Schema({
    title: { type: String, es_type: "text", es_boost: 2 },
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now, es_type: "date" },
    children: mongoose.Schema.Types.Mixed,
    categories: [String],
    menuId: mongoose.Schema.Types.ObjectId,
    content: { type: String, es_type: "text", es_boost: 1 },
    summary: String,
    uri: String,
    size: Number,
    type: String,
    component: String,
    owner: {
        type: Number,
        index: true,
        ref: "User",
        es_type: {
            name: { es_type: "string" },
            photoURL: { es_type: "string" },
            email: { es_type: "string" },
            _id: { es_type: "integer" }
        }
    },
    parentBundleId: { type: Number, index: true },
    rootBundleId: { type: Number, index: true },
    appId: {
        type: Number,
        index: true,
        ref: "App",
        es_type: {
            _id: { es_type: "integer" },
            publicState: { es_type: "integer" },
            name: { es_type: "string" },
            build: {
                es_type: { appName: { es_type: "string" }, iconImage: { es_type: { url: { es_type: "string" } } } },                
            }
        }
    },
    stats: {
        view: { type: Number, default: 0 },
        reply: { type: Number, default: 0 },
        like: { type: Number, default: 0 }
    },
    fakeStats: {
        view: { type: Number, default: 0 },
        reply: { type: Number, default: 0 },
        like: { type: Number, default: 0 }
    },
    actionUsers: {
        view: [mongoose.Schema.Types.Mixed],
        like: [Number]
    },
    thumbnail: String,
    deleted: { type: Boolean, default: false }
});

// bundleSchema.plugin(mexp, {
//     hosts: [
//         "https://search-weneepl-search-jlyvnf4krepxk6daf7qp7i2w6q.ap-northeast-2.es.amazonaws.com"
//     ]
// });
// bundleSchema.plugin(mongoosePaginate);
bundleSchema.plugin(autoIncrement.plugin, {
    model: "Bundle",
    startAt: 10000,
    field: "_id"
});

var Bundle = mongoose.model("Bundle", bundleSchema);

function indexing() {
    console.log("indexing start");
    let count = 0;
    Bundle.on("es-bulk-sent", function() {});

    Bundle.on("es-bulk-data", function(doc) {
        ++count;
    });

    Bundle.on("es-bulk-error", function(err) {
        console.log("err - ", err);
    });
    let bundleQuery = Bundle.find({
        deleted: false,
        component: "article",
        "children.allowVisible": true
    })
        .populate("appId")
        .populate("owner");
    Bundle.esSynchronize(bundleQuery).then(() => {
        console.log("indexed " + count + " documents!");
    });
}

module.exports = Bundle;
module.exports.indexing = indexing;
