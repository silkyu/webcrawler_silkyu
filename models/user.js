var mongoose = require("mongoose");
const config = require("../config.js");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);

var connection = mongoose.connection;

var UserSchema = mongoose.Schema({
    userKey: String,
    fcmToken: String,
    platform: String,
    versionCodeCV: Number,
    versionCode: String,
    versionName: String,
    flightDataVer: Number,

    searchCount: { type: Number, default: 0},
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model("User", UserSchema);
