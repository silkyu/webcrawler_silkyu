var mongoose = require("mongoose");
const config = require("../config.js");

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);

var connection = mongoose.connection;

var zoominfoSchema = mongoose.Schema({
    airline : String,
    areaName : String,
    url : String,
    fileURL : String,
    fileName : String,
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model("ZoomInfo", zoominfoSchema);
