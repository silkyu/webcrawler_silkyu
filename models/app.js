var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");
// var async = require("async");

var serverconfig = require("../config");
var state = require("../state.js");

// var Menu = require("./menu");

const PUBLIC_STATE = {
    private : 0,
    public : 1
};

const MEMBER_APPROVE_TYPE = {
    none: 0,
    must: 1,
    following: 2
};

const MYHOME_TEMPLATE_ID = mongoose.Types.ObjectId('5c22d1165c61284f025958bf');

mongoose.connect(
    serverconfig.mongodbUri(), {useNewUrlParser:true}
);
var connection = mongoose.connection;
autoIncrement.initialize(connection);

var appSchema = mongoose.Schema({
    appBundleId: {
        type: String
    },
    build: mongoose.Schema.Types.Mixed,
    publicState: { type: Number, default: PUBLIC_STATE.private},
    robotAccess: { type: Boolean, default: false},
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    lastUpdated: { type: Date, default: Date.now },
    needEmailVerification: { type: Boolean, default: false },
    groups: [String],
    ownerId: { type: Number, index: true },
    adminId: [{ type: Number }],
    templateId: { type: String },
    hashName: { type: String },
    hashVersionCode: {type: Number },
    children: mongoose.Schema.Types.Mixed,
    onesignal_api: {
        ios: {
            apiKey: {
                type: String,
                default: "N2Q2MGNiZjEtOTA1Yi00NDE5LTk1MmYtODMxYmU5YzQ3MTk5"
            },
            appId: {
                type: String,
                default: "740d5606-f56c-4fd1-8f30-ec75b9ed2bbf"
            }
        },
        android: {
            apiKey: {
                type: String,
                default: "N2Q2MGNiZjEtOTA1Yi00NDE5LTk1MmYtODMxYmU5YzQ3MTk5"
            },
            appId: {
                type: String,
                default: "740d5606-f56c-4fd1-8f30-ec75b9ed2bbf"
            }
        }
    },
    approveType: { type: Number, default: 0 },
    data_version: Number,
    engine_version: { type: Number, default: 1 },
    splashImg: String,
    subscriptions: [
        {
            code: String,
            start: Date,
            end: Date,
            isFree: { type: Boolean, default: false }
        }
    ],
    openStatus: { type: String, default: state.OPEN_STATUS.testApp },
    popups: [
        {
            image: String,
            link: String,
            enable: Boolean
        }
    ],
    hashName: String,
    hashVersionCode: Number,
    stats: {
        view: { type: Number, default: 0 },
        reply: { type: Number, default: 0 },
        like: { type: Number, default: 0 },
        article: { type: Number, default: 0 },
        visit: { type: Number, default: 0 },
        member: { type: Number, default: 0 }
    },
    skinId: mongoose.Schema.Types.ObjectId,
    homeType: String,
    storyRecommendRate: {type:Number, default:1},
    pushTokens: [String],
    version: {
        android: {
            major: {type: Number, default: 1},
            minor: {type: Number, default: 1}
        },
        ios: {
            major: {type: Number, default: 1},
            minor: {type: Number, default: 1}
        },
        web: {
            major: {type: Number, default: 1},
            minor: {type: Number, default: 1}
        }
    },
    sitemap: {type:Boolean, default:false}
});
appSchema.plugin(autoIncrement.plugin, {
    model: "App",
    startAt: 10000,
    field: "_id"
});

appSchema.statics.setNumArticle = function(appId, value) {
    this.findByIdAndUpdate(appId, {$set:{lastUpdated:new Date(), "stats.article":value}}, (err) => {

    });
}

module.exports = mongoose.model("App", appSchema);
module.exports.PUBLIC_STATE = PUBLIC_STATE;
module.exports.MEMBER_APPROVE_TYPE = MEMBER_APPROVE_TYPE;
module.exports.MYHOME_TEMPLATE_ID = MYHOME_TEMPLATE_ID;