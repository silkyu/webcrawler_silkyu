const puppeteer = require('puppeteer');
var cheerio = require('cheerio');
var express = require('express');
var app = express();
var cors = require("cors");
const bodyParser = require('body-parser');
var request = require('request');
var fs = require('fs');
var XLSX = require('xlsx');
// const App = require("./models/app");
// const Bundle = require("./models/bundle");
// const User = require('./models/user');
// const Menu = require('./models/menu');
const AsianaSeat = require('./models/asianaseat')
const KoreanSeat = require('./models/koreanseat')
const SearchLog = require('./models/searchlog')
const User = require('./models/user')
const ZoomInfo = require('./models/zoominfo')

var mongoose = require("mongoose");
const config = require("./config.js");
const {dateToYYYYMMDD, dateToYYYYMMDDWith, convert2DateMMDDYYYY, myTimeout} = require("./util.js")
const {AIRPORT_INFO, AIRPORT_INFO_KOREAN, FLIGHT_INFO, FLIGHT_INFO_KOREAN} = require("./airportinfo.js")

var program = require("commander");

var zoomCityMatching = {};
var zoomDataObj = {};

var port = 8089;
app.use(cors());
app.options("*", cors());
app.use(bodyParser.json({ limit: "5mb" }));
app.use(bodyParser.urlencoded({ extended: false, limit: "5mb" }));
app.get('/ping', (req, res) => {
    res.sendStatus(200);
});

function openWebServer() {
    console.log('listen port : ' + port);
    app.listen(port);
}

const mileUseButton = 'div.quick_tab_wrap > div.mile_use > div > label';
// const arrivalAPR = '#arrivalAirportR';
// const arrivalCityR = '#arrivalCityR';
// const arrivalAreaR = '#arrivalAreaR';
// const arrivalCityNameR = '#arrivalCityNameR';

app.get('/', function(req, res){
    console.log('app.get root!!!!');
    res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
    res.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">');
    res.write('<form action="search_seat" method="post">');
    res.write('출발도시 : ');
    res.write('<select name="departure"><option value="ICN">서울</option></select><br>');
    res.write('도착도시 : ');
    res.write('<select name="arrival">');
    var cities = [];
    for(var areaKey in AIRPORT_INFO) {
        console.log('areaKey = ', areaKey);
        cities = cities.concat(AIRPORT_INFO[areaKey]);
    }
    // var cities = AIRPORT_INFO['US'];
    // console.log('AIRPORT_INFO : ', AIRPORT_INFO);
    // console.log('cities : ', cities);
    for(let i = 0; i < cities.length; i++) {
        res.write('<option value="' + cities[i].airport + '">' + cities[i].cityName + '</option>');
    }
    res.write('</select><br>');
    res.write('좌석수 : ');
    res.write('<select name="seatNum">');
    for(let i = 1; i < 9; i++) {
        res.write('<option value="' + i + '">' + i + '개</option>');
    }
    res.write('</select>');
    res.write('좌석종류 : ');
    res.write('<select name="seatType">');
        res.write('<option value="total">좌석무관</option>');
        res.write('<option value="economy">이코노미</option>');
        res.write('<option value="business">비지니스</option>');
        res.write('<option value="first">일등석</option>');
    res.write('</select><br>');
    res.write('검색최소박수 : ');
    res.write('<select name="minNight">');
    for(let i = 1; i < 10; i++) {
        res.write('<option value="' + i + '">' + i + '박</option>');
    }
    res.write('</select><br>');
    res.write('검색최대박수 : ');
    res.write('<select name="maxNight">');
    for(let i = 1; i < 10; i++) {
        res.write('<option value="' + i + '">' + i + '박</option>');
    }
    res.write('</select><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
});

//YYYYMMDD -> YYYY년 MM월 DD일(요일)
function getStdDateExpress(dateStr) {
    var dayStrs = ['일', '월', '화', '수', '목', '금', '토'];
    var YYYY = dateStr.substring(0, 4);
    var MM = dateStr.substring(4, 6);
    var DD = dateStr.substring(6, 8);
    var inputDate = new Date(YYYY, parseInt(MM) - 1, parseInt(DD));
    console.log('dateStr : ' + dateStr);
    console.log('inputDate : ' + inputDate);
    return YYYY + '년 ' + MM + '월 ' + DD + '일(' + dayStrs[inputDate.getDay()] + ')';
}

app.post('/search_seat', function(req, res) {
    // console.log('req = ',  req);
    console.log('req.body = ',  req.body);
    let minNight = parseInt(req.body.minNight);
    let maxNight = parseInt(req.body.maxNight);
    let departure = req.body.departure;
    let arrival = req.body.arrival;
    let seatNum = parseInt(req.body.seatNum);
    let seatType = req.body.seatType;
    let searchStart = req.body.searchStart;
    let searchEnd = req.body.searchEnd;
    let arrivalCities = req.body.arrivalCities;
    
    res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
    res.write('출발지 = ' + departure + '<br>');
    res.write('도착지 = ' + arrival + '<br>');
    res.write('최소박수 = ' + minNight + '박<br>');
    res.write('최대박수 = ' + maxNight + '박<br>');
    res.write('좌석수 = ' + seatNum + '(' + seatType + ')<br>');
    res.write('검색도시 = ' + arrivalCities + '<br>');
    if(searchStart && searchEnd) { 
        res.write('검색기간 = ' + searchStart + ' ~ ' + searchEnd + '<br>');
    }
    res.write('<br>');
    var seatCondition = {
        isKorean: 0,
        isFlyRound: 1,
        departure: departure,
        arrival: arrival,
        seatType: seatType,
        seatNum: seatNum,
        minNight: minNight,
        maxNight: maxNight,
        searchStart: searchStart,
        searchEnd: searchEnd,
    }
    getAvailSeatData(0, 1, departure, arrival, seatType, seatNum, minNight, maxNight, searchStart, searchEnd).then((result) => {
        var curDate = new Date();
        var departureUpdated = new Date(result['depatureUpdated']);
        var arrivalUpdated = new Date(result['arrivalUpdated']);
        var diffDeparture = curDate - departureUpdated;
        var diffArrival = curDate - arrivalUpdated;
        console.log('diffDeparture = ', diffDeparture);
        console.log('diffArrival = ', diffArrival);
        var hourUnit = 1000*60*60;
        res.write('데이터 업데이트(' + departure + '->' + arrival+') : ' + departureUpdated + '(' + parseInt(diffDeparture/hourUnit) + '시간 전)<br>');
        res.write('데이터 업데이트(' + arrival + '->' + departure +') : ' + arrivalUpdated + '(' + parseInt(diffArrival/hourUnit) + '시간 전)<br>');
        res.write('<br>');
        for(let night = minNight; night <= maxNight; night++) {
            res.write('<' + night + '박 ' + (night+1) + '일><br>')
            for(let i = 0; i < result[night].length; i++) {
                let curResult = result[night][i];
                res.write('[출발] ' + getStdDateExpress(curResult.departureDate) + ' ' + curResult.departureTime + ', 이코노미 ' + curResult.departureEconomy + '석, 비즈니스 ' + curResult.departureBusiness + '석, 퍼스트 ' + curResult.departureFirst + '석<br>');
                res.write('[도착] ' + getStdDateExpress(curResult.arrivalDate) + ' ' + curResult.arrivalTime + ', 이코노미 ' + curResult.arrivalEconomy + '석, 비즈니스 ' + curResult.arrivalBusiness + '석, 퍼스트 ' + curResult.arrivalFirst + '석<br>');
                res.write('<br>');
            }
        }
        res.end();
    }).catch((err) => {
        res.end();
    });
});

const DAYOFWEEK_INFO = {
    'sun' : 0,
    'mon' : 1,
    'tue' : 2,
    'wed' : 3,
    'thu' : 4,
    'fri' : 5,
    'sat' : 6,
};

function getConvertVersionCode(platform, versionCode) {
    let convertedCode = -1;
    if(platform == 'ios') {
        if(versionCode) {
            convertedCode = 0; 
            versionCodeSplit = versionCode.split('.');
            let multiple = 1;
            for(let i = versionCodeSplit.length-1; i >= 0; i--) {
                let codeInt = parseInt(versionCodeSplit[i]);
                if(i == 0 && i < versionCodeSplit.length-1) {
                    codeInt = Math.max(0, codeInt - 1);
                }
                convertedCode = convertedCode + codeInt*multiple;
                multiple = multiple * 10;
            }
            if(convertedCode >= 1) {
                convertedCode = 6;
            }
        }
    } else if(platform == 'android') {
        if(versionCode) {
            convertedCode = parseInt(versionCode);
        }
    }
    console.log('platform %s, versionCode %s, convertedCode %i', platform, versionCode, convertedCode);
    return convertedCode;
}

app.get('/get_flight_info', function(req, res) {
    var CUR_SERVER_DATA_VER = 3;
    var curClientDataVer = 0;
    if(req.query.dataver) {
        var curClientDataVer = parseInt(req.query.dataver);
    }
    var retJSON = {};
    console.log('curClientDataVer = ',curClientDataVer);
    retJSON['needUpdate'] = (curClientDataVer !== CUR_SERVER_DATA_VER);
    if(retJSON['needUpdate']) {
        let flightInfo = {};
        flightInfo['dataver'] = 1;
        flightInfo['asiana'] = FLIGHT_INFO;
        flightInfo['korean'] = FLIGHT_INFO_KOREAN;
        retJSON['flightInfo'] = flightInfo;
    }
    res.json(retJSON);
    res.end();
});

app.post('/search_seat_json', function(req, res) {
    // console.log('req = ',  req);
    console.log('req.body = ',  req.body);
    let userKey = req.body.userKey;
    let adLoaded = req.body.adLoaded;
    let minNight = parseInt(req.body.minNight);
    let maxNight = parseInt(req.body.maxNight);
    let departure = req.body.departure;
    let arrival = req.body.arrival;
    let seatNum = parseInt(req.body.seatNum);
    let seatType = req.body.seatType;
    let searchStart = req.body.searchStart;
    let searchEnd = req.body.searchEnd;
    let departDay = req.body.departDay;
    let arriveDay = req.body.arriveDay;
    let holidayNum = req.body.holidayNum;
    let mileageLimit = req.body.mileageLimit;
    let departureCities = req.body.departureCities;
    let arrivalCities = req.body.arrivalCities;
    let isFlyRound = req.body.isFlyRound;
    let isKorean = req.body.isKorean;
    let platform = req.body.platform;
    let versionName = req.body.versionName;
    let versionCode = req.body.versionCode;
    let fcmToken = req.body.fcmToken;
    let flightDataVer = req.body.flightDataVer;

    if(!mileageLimit) {
        mileageLimit = 9999999;
    }
    if(!departureCities || departureCities.length == 0) {
        departureCities = [departure];
    }
    if(!arrivalCities || arrivalCities.length == 0) {
        arrivalCities = [arrival];
    }

    // if(!versionCode) {
    //     versionCode = -1;
    // }

    let convertedVersionCode = getConvertVersionCode(platform, versionCode);

    var newDepartDay = [false, false, false, false, false, false, false];
    var newArriveDay = [false, false, false, false, false, false, false];
    for(let i = 0; i < departDay.length; i++) {
        if(DAYOFWEEK_INFO[departDay[i]] !== undefined) {
            newDepartDay[DAYOFWEEK_INFO[departDay[i]]] = true;
        }
    }
    for(let i = 0; i < arriveDay.length; i++) {
        if(DAYOFWEEK_INFO[arriveDay[i]] !== undefined) {
            newArriveDay[DAYOFWEEK_INFO[arriveDay[i]]] = true;
        }
    }

    if(searchStart.indexOf('.') >= 0) {
        searchStartSplit = searchStart.split('.');
        searchStart = searchStartSplit[0] + (searchStartSplit[1].length<2?'0':'') + searchStartSplit[1] + (searchStartSplit[2].length<2?'0':'') + searchStartSplit[2];
    }
    if(searchEnd.indexOf('.') >= 0) {
        searchEndSplit = searchEnd.split('.');
        searchEnd = searchEndSplit[0] + (searchEndSplit[1].length<2?'0':'') + searchEndSplit[1] + (searchEndSplit[2].length<2?'0':'') + searchEndSplit[2];
    }
    // if(!flightDataVer) {
    //     flightDataVer = -1;
    // }
    var seatCondition = {
        userKey: userKey,
        adLoaded: adLoaded,
        isKorean: isKorean,
        isFlyRound: isFlyRound,
        departure: departure,
        arrival: arrival,
        departureCities: departureCities,
        arrivalCities: arrivalCities,
        seatType: seatType,
        seatNum: seatNum,
        minNight: minNight,
        maxNight: maxNight,
        searchStart: searchStart,
        searchEnd: searchEnd,
        departDay: newDepartDay,
        arriveDay: newArriveDay,
        mileageLimit: mileageLimit,
        platform: platform,
        versionName: versionName,
        versionCode: versionCode,
        versionCodeCV: convertedVersionCode,
        fcmToken: fcmToken,
        flightDataVer: flightDataVer,
    }

    console.log('seatCondition = ', seatCondition);
    processSearch(res, seatCondition);
    // (async () => {
    //     // let resData = {result: false, errMsg:'이 버전은 더이상 사용하실 수 없습니다. 마켓에서 새로운 버전으로 업데이트해주세요.'};
    //     // res.json(resData);
    //     // res.end();
    //     let resData = {result: true, dataNum:0, data:[]};
    //     try {
    //         let result = await getAvailSeatData(seatCondition);
    //         console.log('RESULT데이터 ' + result['scData'].length + '개 완료!');
    //         resData.data.push(result);
    //     } catch(err) {
    //         console.log(err);
    //     }
    //     resData.dataNum = resData.data.length;
    //     console.log('RESPONSE ' + resData.dataNum + '세트 전송!');
    //     res.json(resData);
    //     res.end();

    //     await saveSearchLog(seatCondition);
    // })();

    
    // getAsiana(departure, arrival, seatType, seatNum, minNight, maxNight).then((result) => {
    //     var resultData = {dataNum:1, data:[]};
    //     console.log('result : ', result);
    //     resultData.data.push(result);
    //     // res.writeHead(200, {'Content-Type': 'application/json'});
    //     res.json(result);
    //     res.end();
    // }).catch((err) => {
    //     console.log('err : ', err);
    //     res.end();
    // });
    // console.log('end');
});

async function processSearch(res, seatCondition) {
    let resData = {result: false, errMsg:'죄송합니다. 내부 사정으로 서비스가 종료되었습니다. 그동안 이용해주셔서 감사합니다.'};
    res.json(resData);
    res.end();
    // let resData = {result: true, dataNum:0, data:[]};
    // try {
    //     let totalNum = seatCondition.departureCities.length * seatCondition.arrivalCities.length;
    //     let isBriefing = (totalNum > 1);
    //     resData['isBriefing'] = isBriefing;
    //     for(let dIdx = 0; dIdx < seatCondition.departureCities.length; dIdx++) {
    //         for(let aIdx = 0; aIdx < seatCondition.arrivalCities.length; aIdx++) {
    //             seatCondition.departure = seatCondition.departureCities[dIdx];
    //             seatCondition.arrival = seatCondition.arrivalCities[aIdx];
    //             console.log(seatCondition.departure + ' > ' + seatCondition.arrival);
    //             let result = await getAvailSeatData(seatCondition, isBriefing);
    //             // console.log('result = ', result);
    //             if((seatCondition.versionCodeCV && seatCondition.versionCodeCV > 3) && result['validFlight'] < 0) {
    //                 if(!isBriefing) {
    //                     if(result['validFlight'] == -1) {
    //                         console.log('존재하지 않는 노선 : ', seatCondition.departure + ' > ' + seatCondition.arrival);
    //                         resData.result = false;
    //                         resData.errMsg = '해당 노선은 운항스케쥴이 없거나 현재 버전에서 지원하지 않는 노선입니다.';
    //                     } else if(result['validFlight'] == -2) {
    //                         console.log('마일리지 부족 : ', seatCondition.departure + ' > ' + seatCondition.arrival + '(' + seatCondition.mileageLimit + ')');
    //                         resData.result = false;
    //                         resData.errMsg = '해당 노선은 설정된 마일리지 한도를 초과합니다.';
    //                     }
    //                 }
    //             } else {
    //                 console.log('RESULT데이터 ' + result['scDataCnt'] + '(' + result['scData'].length + ')개 완료!');
    //                 if(!isBriefing || result['scDataCnt'] > 0) {
    //                     resData.data.push(result);
    //                 }
    //             }
    //         }
    //     }
    // } catch(err) {
    //     console.log(err);
    // }
    // resData.dataNum = resData.data.length;
    // console.log('RESPONSE ' + resData.dataNum + '세트 전송!');

    // resData['resultMsg'] = '새로운 버전이 업데이트되었습니다. ' + (seatCondition.platform=='ios'?'앱스토어':'구글플레이') + '에서 업데이트 해주세요.';
    // resData['resultPopup'] = true;
    // resData['resultUrl'] = 'market://details?id=com.weneepl.pottrip';


    // res.json(resData);
    // res.end();

    // await saveSearchLog(seatCondition);
    // // if(!seatCondition.fcmToken) {
    // //     seatCondition.fcmToken = 'null';
    // // }    // if(seatCondition.fcmToken && seatCondition.fcmToken.length > 0) {
    //     await saveUser(seatCondition);
    // // }
}

program
.version('1.0.1')
.option('--crawla', 'crawl asiana')
.option('--geta', 'get data asiana')
.option('--crawlk', 'crawl koreana')
.option('--crawlkakorea', 'crawl korean/asiana korea')
.parse(process.argv);

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);
mongoose.Promise = global.Promise;

var dbConnectCompleted = false;
var db = mongoose.connection;
//데이터베이스 접속 확인
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function(callback) {
    console.log("mongo DB connected...");
    dbConnectCompleted = true;
});

var today = new Date();
var year = today.getFullYear();
console.log('timeInMs = ', year);
console.log('today = ', today);


// writeRobotArticle('로봇테스트', [{text:'로봇본문1'}, {text:'로봇본문2'}, {text:'로봇본문3'}], [], [], [], 0);

// var curDateStr = '20190218'
// for (let i = 1; i < 500 ; i++) {
//     let newDateStr = addDateStr(curDateStr , i);
//     console.log(newDateStr);
// }

function addDateStr(dateStr, add) {
    let stdDate = new Date(parseInt(dateStr.substring(0, 4)), parseInt(dateStr.substring(4, 6))-1, parseInt(dateStr.substring(6, 8)));
    stdDate.setDate(stdDate.getDate() + add);
    return dateToYYYYMMDD(stdDate);
}

if (program.crawla) {
    crawlAsiana();
    // crawlAsianaArea('EA');
    // crawlAsianaCity('EA', AIRPORT_INFO['EA'][4]);
} else if(program.geta) {
    var seatCondition = {
        isKorean: 0,
        isFlyRound: 1,
        departure: 'ICN',
        arrival: AIRPORT_INFO.cities[3].airport,
        seatType: 'total',
        seatNum: 2,
        minNight: 4,
        maxNight: 6,
        searchStart: '20190101',
        searchEnd: '20210101',
    }

    getAvailSeatData(seatCondition).then((result) => {
        console.log('result = ', result);
    });
} else if (program.crawlk) {
    // crawlZoomKorean();
    // testCrawlKoreanArea('GUM');
    crawlKoreanAir();
} else if (program.crawlkakorea) {
    searchKoreanFlightInfoAll();
    // crawlAreaKorea();
} else {
    openWebServer();
    getDayOfWeek('20190305');
}

async function searchKoreanFlightInfoAll() {
    var searchDate = new Date();
    var finalSearchDate = new Date();
    finalSearchDate.setDate(finalSearchDate.getDate()+180);
    var searchDateStr = dateToYYYYMMDDWith(searchDate);
    var finalSearchDateStr = dateToYYYYMMDDWith(finalSearchDate);
    var departureList = ['ICN', 'GMP', 'PUS'];
    var finalObj = {};
    for(let dIdx = 0; dIdx < departureList.length; dIdx++) {
        let departure = departureList[dIdx];
        let departureObj = {};
        for(let area in AIRPORT_INFO_KOREAN) {
            let cities = AIRPORT_INFO_KOREAN[area];
            for(let cityIdx = 0; cityIdx < cities.length; cityIdx++) {
                if(cities[cityIdx].isGroup) continue;
                let arrival = cities[cityIdx].airport;
                let cityName = cities[cityIdx].cityName;
                if(arrival != departure) {
                    let result = await getKoreanFlightInfo(departure, arrival, searchDateStr, finalSearchDateStr);
                    if(result) {
                        if(!departureObj[area]) {
                            departureObj[area] = [];
                        }
                        departureObj[area].push({'airport':arrival, 'cityName':cityName});
                    }
                }
            }
        }
        finalObj[departure] = departureObj;
    }
    console.log('{')
    for(let departure in finalObj) {
        console.log('\t' + '\'' + departure + '\': {')
        for(let area in finalObj[departure]) {
            let cities = finalObj[departure][area];
            console.log('\t' + '\t' + '\'' + area + '\': [')
            for(let cityIdx = 0; cityIdx < cities.length; cityIdx++) {
                console.log('\t' + '\t' + '\t' + '{ airport: \'' + cities[cityIdx].airport + '\', cityName: \'' + cities[cityIdx].cityName + '\' },');
                // console.log(cities[cityIdx]);
            }
            console.log('\t' + '\t' + '], ');
        }   
        console.log('\t' + '},');
    }
    console.log('}');
}

async function getKoreanFlightInfo(fromCityCode, toCityCode, searchDateStr, finalSearchDateStr) {
    let monthArray = getMonthArray(searchDateStr, finalSearchDateStr);
    for(let i = 0; i < monthArray.length; i++) {
        let resultRes = await requestUrlPost('https://kr.koreanair.com/api/viewBonusSeat/monthly', '{"departureCityApo":"' + fromCityCode + '","arrivalCityApo":"' + toCityCode + '","awardBookingClass":"X","desiredTravelDate":"' + monthArray[i][1] + '","desiredTravelMonth":"' + monthArray[i][0] + '","tripType":"outbound"}');
        let resultObj = resultRes['viewSeatLists'];
        // console.log('resultRes = ', resultRes);
        // console.log('resultObj = ', resultObj);
        if(resultObj && resultObj.length > 0) {
            console.log(fromCityCode + ' > ' + toCityCode + ' : 운항정보 있음.')
            return true;
        }
     }
     console.log(fromCityCode + ' > ' + toCityCode + ' : 운항하지 않는 노선입니다.')
     return false;
}

function getUpdatedTime(airline, departureId, arrivalId) {
    var DBObject;
    if(airline == 'KOREAN') {
        DBObject = KoreanSeat;
    } else {
        DBObject = AsianaSeat;
    }
    return new Promise((resolve, reject) => {
        DBObject.findOne({departureId:departureId, arrivalId:arrivalId}, (err, data) =>{
            if(err) {
                resolve(undefined);
            } else {
                if(data) {
                    resolve(data.updated);
                }
                resolve(undefined);
            }
        });
    });
}

function saveSearchLog(seatCondition) {
    return new Promise((resolve, reject) => {
        var searchLog = new SearchLog();
        searchLog.userKey = seatCondition.userKey;
        searchLog.adLoaded = seatCondition.adLoaded;
        searchLog.departureId = seatCondition.departure;
        searchLog.arrivalId = seatCondition.arrival;
        searchLog.isKorean = seatCondition.isKorean;
        searchLog.isFlyRound = seatCondition.isFlyRound;
        searchLog.minNight = seatCondition.minNight;
        searchLog.maxNight = seatCondition.maxNight;
        searchLog.searchStart = seatCondition.searchStart;
        searchLog.searchEnd = seatCondition.searchEnd;
        searchLog.departDay = seatCondition.departDay;
        searchLog.arriveDay = seatCondition.arriveDay;
        // console.log('searchLog - ', searchLog);
        searchLog.save(function(err, searchLog) {
            if (err) {
                reject(err);
            } else {
                resolve(searchLog);
            }
        });
    });
}

function saveUser(seatCondition) {
    var setObject = {
        "platform" : seatCondition.platform,
        "versionCodeCV" : seatCondition.versionCodeCV,
        "versionCode" : seatCondition.versionCode,
        "versionName" : seatCondition.versionName,
    };
    if(seatCondition.flightDataVer) {
        setObject['flightDataVer'] = parseInt(seatCondition.flightDataVer);
    }
    if(seatCondition.fcmToken) {
        setObject['fcmToken'] = seatCondition.fcmToken;
    }
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate(
            {
                "userKey" : seatCondition.userKey,
            },
            {
                $inc: {
                    searchCount: 1
                },
                $set : setObject
            },
            {
               upsert : true,
               new:true
            },
            (err, newSeat) => {
                console.log('err - ', err);
                // console.log('newSeat - ', newSeat);
            }
        );
    });
}

function getValidFlightDB(seatCondition) {
    var DBObject = seatCondition.isKorean?KoreanSeat:AsianaSeat;
    return new Promise((resolve, reject) => {
        DBObject.findOne({departureId: seatCondition.departure, arrivalId: seatCondition.arrival}, (err, datas) => {
            if(err) {
                reject(err);
            } else {
                resolve(datas);
            }
        });
    });
}

function getAvailKoreanSeat(seatCondition) {
    var matchExpression;
    if (seatCondition.seatType == 'economy') {
        matchExpression = { $match: {economy: {$gte:seatCondition.seatNum}} };
    } else if (seatCondition.seatType == 'business') {
        matchExpression = { $match: {business: {$gte:seatCondition.seatNum}} };
    } else if (seatCondition.seatType == 'first') {
        matchExpression = { $match: {first: {$gte:seatCondition.seatNum}} };
    } else {
        matchExpression = { $match: {total: {$gte:seatCondition.seatNum}} };
    }
    matchExpression['$match']['sdate'] = { $gte:seatCondition.searchStart, $lte:seatCondition.searchEnd };
    // console.log('matchExpression : ', matchExpression);
    return new Promise((resolve, reject) => {
        KoreanSeat.aggregate([
            {
                $match: {departureId: seatCondition.departure, arrivalId: seatCondition.arrival}
            },
            {
                $unwind: '$seatState'
            },
            {
                $project: {
                    updated: '$updated',
                    departure: '$departureName',
                    arrival: '$arrivalName',
                    sdate: '$seatState.sdate',
                    stime: '$seatState.stime',
                    aircraftType: '$seatState.aircraftType',
                    economy: { $arrayElemAt: ["$seatState.seatNum", 0] },
                    business: { $arrayElemAt: ["$seatState.seatNum", 1] },
                    first: { $arrayElemAt: ["$seatState.seatNum", 2] },
                    total: { $sum : ["$seatState.seatNum"] }
                }
            },
            matchExpression],
            (err, datas) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(datas);
                }
            }
        );
    });
}

function getAvailAsianSeat(seatCondition) {
    var matchExpression;
    if (seatCondition.seatType == 'economy') {
        matchExpression = { $match: {economy: {$gte:seatCondition.seatNum}} };
    } else if (seatCondition.seatType == 'business') {
        matchExpression = { $match: {business: {$gte:seatCondition.seatNum}} };
    } else if (seatCondition.seatType == 'first') {
        matchExpression = { $match: {first: {$gte:seatCondition.seatNum}} };
    } else {
        matchExpression = { $match: {total: {$gte:seatCondition.seatNum}} };
    }
    matchExpression['$match']['sdate'] = { $gte:seatCondition.searchStart, $lte:seatCondition.searchEnd };
    // console.log('matchExpression : ', matchExpression);
    return new Promise((resolve, reject) => {
        AsianaSeat.aggregate([
            {
                $match: {departureId: seatCondition.departure, arrivalId: seatCondition.arrival}
            },
            {
                $unwind: '$seatState'
            },
            {
                $project: {
                    updated: '$updated',
                    departure: '$departureName',
                    arrival: '$arrivalName',
                    sdate: '$seatState.sdate',
                    stime: '$seatState.stime',
                    flightTime: '$seatState.flightTime',
                    aircraftType: '$seatState.aircraftType',
                    economy: { $arrayElemAt: ["$seatState.seatNum", 0] },
                    business: { $arrayElemAt: ["$seatState.seatNum", 1] },
                    first: { $arrayElemAt: ["$seatState.seatNum", 2] },
                    total: { $sum : ["$seatState.seatNum"] }
                }
            },
            matchExpression],
            (err, datas) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(datas);
                }
            }
        );
    });
}

function getDayOfWeek(dateStr) {
    var dateObj = new Date();
    dateObj.setFullYear(parseInt(dateStr.substring(0, 4)));
    dateObj.setMonth(parseInt(dateStr.substring(4, 6))-1);
    dateObj.setDate(parseInt(dateStr.substring(6, 8)));
    var dayOfWeek = dateObj.getDay();
    return dayOfWeek;
}

function getFlightInfo(isKorean, departure, arrival) {
    // console.log('departure = ', departure);
    // console.log('arrival = ', arrival);
    let flightInfo;
    if(isKorean) {
        flightInfo = FLIGHT_INFO_KOREAN;
    } else {
        flightInfo = FLIGHT_INFO;
    }
    if(!flightInfo[departure]) return null;
    for(let area in flightInfo[departure]) {
        let cities = flightInfo[departure][area];
        for(let cityIdx = 0; cityIdx < cities.length; cityIdx++) {
            if(cities[cityIdx].airport == arrival) {
                return cities[cityIdx];
            }
        }
    }
    return null;
}

// async function getAvailSeatData(isKorean, isFlyRound, departureId, arrivalId, seatType, minSeat, minNight, maxNight, searchStartDate, searchEndDate) {
async function getAvailSeatData(seatCondition, isBriefing) {
    if(!seatCondition.minNight || seatCondition.minNight <= 0) {
        seatCondition.minNight = 1;
    }
    if(!seatCondition.maxNight || seatCondition.maxNight < seatCondition.minNight) {
        seatCondition.maxNight = seatCondition.minNight;
    }

    var retVal = {};
    retVal['departureCode'] = seatCondition.departure;
    retVal['arrivalCode'] = seatCondition.arrival;
    retVal['isFlyRound'] = seatCondition.isFlyRound;
    retVal['scData'] = [];
    retVal['scDataCnt'] = 0;

    var availableDepart = {};
    var availableArrival = {};
    if(!dbConnectCompleted) {
        await myTimeout(3000).then(() => {
            console.log('setTimeout completed');
        });
    }
    var flightDBInfo = await getValidFlightDB(seatCondition);
    // console.log('flightDBInfo = ', flightDBInfo);
    var dataLength = 0;
    if(flightDBInfo) {
        dataLength = flightDBInfo['seatState'].length;
    }
    if(dataLength == 0) {
        retVal['validFlight'] = -1;
        return retVal;
    } else {
        retVal['validFlight'] = 1;
    }
    var flightInfo = getFlightInfo(seatCondition.isKorean, seatCondition.departure, seatCondition.arrival);
    if(!flightInfo) {
        flightInfo = getFlightInfo(seatCondition.isKorean, seatCondition.arrival, seatCondition.departure);
    }
    // console.log('flightInfo = ', flightInfo);

    if(flightInfo) {
        let multiple = seatCondition.seatNum;
        if(!seatCondition.isFlyRound) {
            multiple = multiple * 0.5;
        }
        retVal['eco'] = flightInfo['eco'] * multiple;
        retVal['biz'] = flightInfo['biz'] * multiple;
        retVal['fir'] = flightInfo['fir'] * multiple;
    }
    //마일리지 한도 검사
    let targetMile = retVal['eco'];
    if(seatCondition.seatType == 'business') {
        targetMile = retVal['biz'];
    } else if(seatCondition.seatType == 'first') {
        targetMile = retVal['fir'];
    }
    if(seatCondition.mileageLimit < targetMile) {
        retVal['validFlight'] = -2;
        return retVal;
    }
    if(seatCondition.mileageLimit < retVal['fir']) {
        retVal['fir'] = 0;
    }
    if(seatCondition.mileageLimit < retVal['biz']) {
        if(seatCondition.seatType == 'total') {
            seatCondition.seatType = 'economy';
        }
        retVal['biz'] = 0;
    }
    if(seatCondition.mileageLimit < retVal['eco']) {
        retVal['eco'] = 0;
    }


    let getFunction = getAvailAsianSeat;
    if(seatCondition.isKorean) {
        getFunction = getAvailKoreanSeat;
    }
    await getFunction(seatCondition).then((datas) => {
        console.log(seatCondition.departure + ' > ' + seatCondition.arrival + ' : ' + datas.length + '개 데이터 검색!');
        for (let j = 0; j < datas.length; j++) {
            let availDay = true;
            if(seatCondition.departDay) {
                let dayOfWeek = getDayOfWeek(datas[j].sdate);
                availDay = seatCondition.departDay[dayOfWeek];
            }
            if(availDay) {
                if(!availableDepart[datas[j].sdate]) {
                    availableDepart[datas[j].sdate] = [];
                }
                availableDepart[datas[j].sdate].push(datas[j]);
            }
            if (!retVal['depatureUpdated']) {
                retVal['depatureUpdated'] = datas[j].updated;
            }
            // console.log(datas[j]);
        }
    }).catch((err) => {
        console.log(err);
    });
    if(seatCondition.isFlyRound) {
        var tempDeparture = seatCondition.departure;
        seatCondition.departure = seatCondition.arrival;
        seatCondition.arrival = tempDeparture;
        // var newSeatCondition = seatCondition;
        // newSeatCondition.depart = seatCondition.arrival;
        // console.log('seatCondition.arrival = ', seatCondition.arrival);
        // console.log('seatCondition.depart = ', seatCondition.depart);
        // console.log('newSeatCondition.arrival = ', newSeatCondition.arrival);
        // console.log('newSeatCondition.depart = ', newSeatCondition.depart);
        // newSeatCondition.arrival = seatCondition.depart;
        // console.log(seatCondition);
        await getFunction(seatCondition).then((datas) => {
            console.log(seatCondition.departure + ' > ' + seatCondition.arrival + ' : ' + datas.length + '개 데이터 검색!');
            for (let j = 0; j < datas.length; j++) {
                let availDay = true;
                if(seatCondition.arriveDay) {
                    let dayOfWeek = getDayOfWeek(datas[j].sdate);
                    availDay = seatCondition.arriveDay[dayOfWeek];
                }
                if(availDay) {
                    if(!availableArrival[datas[j].sdate]) {
                        availableArrival[datas[j].sdate] = [];
                    }
                    availableArrival[datas[j].sdate].push(datas[j]);
                }
                if (!retVal['arrivalUpdated']) {
                    retVal['arrivalUpdated'] = datas[j].updated;
                }
                // console.log(datas[j]);
            }
        }).catch((err) => {
            console.log(err);
        });
        tempDeparture = seatCondition.departure;
        seatCondition.departure = seatCondition.arrival;
        seatCondition.arrival = tempDeparture;
    }    

    for (var keyDate in availableDepart) {
        var data = availableDepart[keyDate];
        // console.log('keyDate = ', keyDate);
        for(let i = 0; i < data.length; i++) {
            if(!seatCondition.isFlyRound) {
                retVal['scDataCnt'] += 1;
                if(!isBriefing) {
                    let pushData = {
                        departureDate: data[i].sdate,
                        departureTime: data[i].stime,
                        departureEconomy: data[i].economy,
                        departureBusiness: data[i].business,
                        departureFirst: data[i].first,
                        departureAircraft: data[i].aircraftType,
                    };
                    if(!seatCondition.isKorean) {
                        pushData.departureLeadTime = data[i].leadTime;
                    }
                    retVal['scData'].push(pushData);
                }
            } else {
                for(let n = seatCondition.minNight; n <= seatCondition.maxNight; n++) {
                    let returnDate = addDateStr(data[i].sdate, n);
                    if(availableArrival[returnDate]) {
                        var returnData = availableArrival[returnDate];
                        retVal['scDataCnt'] += returnData.length;
                        if(!isBriefing) {
                            for(let j = 0; j < returnData.length; j++) {
                                // console.log(n + '박 ' + (n+1) + '일');
                                let pushData = {
                                    nightNum: n,
                                    departureDate: data[i].sdate,
                                    departureTime: data[i].stime,
                                    departureEconomy: data[i].economy,
                                    departureBusiness: data[i].business,
                                    departureFirst: data[i].first,
                                    departureAircraft: data[i].aircraftType,
                                    arrivalDate: returnData[j].sdate,
                                    arrivalTime: returnData[j].stime,
                                    arrivalEconomy: returnData[j].economy,
                                    arrivalBusiness: returnData[j].business,
                                    arrivalFirst: returnData[j].first,
                                    arrivalAircraft: returnData[j].aircraftType,
                                }
                                if(!seatCondition.isKorean) {
                                    pushData.departureLeadTime = data[i].leadTime;
                                    pushData.arrivalLeadTime = returnData[j].leadTime;
                                }            
                                retVal['scData'].push(pushData);
                                if(retVal['scData'].length > 500) return retVal;
                                // console.log('[출발]날짜 = %s, 시간 = %s, 이코노미 = %i, 비즈니스 = %i, 퍼스트 = %i', data[i].sdate, data[i].stime, data[i].economy, data[i].business, data[i].first);
                                // console.log('[도착]날짜 = %s, 시간 = %s, 이코노미 = %i, 비즈니스 = %i, 퍼스트 = %i', returnData[j].sdate, returnData[j].stime, returnData[j].economy, returnData[j].business, returnData[j].first);
                            }
                        }
                    } else {
                        // console.log('도착비행기편 없음');
                    }
                }
            }
            // var returnMinDate = addDate(availableDepart[i].sdate, minNight);
            // var returnMaxDate = addDate(availableDepart[i].sdate, maxNight);
        }
    }
    return retVal;
}

function requestUrlPost(url, postData) {
    return new Promise((resolve, reject) => {
        // console.log('postData = ', postData);
        // resolve(postData);
        request(
            {
                headers: {
                    'content-type': 'application/json',
                },
                url: url,
                method: 'POST',
                body: postData
            }, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(bodyObj);
                }
            }
        );
    });
}

function requestUrlGet(url, headers, isFile) {
    var requestObj = {
        url: url,
        method: 'GET',
    }
    if(isFile) {
        requestObj['encoding'] =  null;
    }
    if(headers) {
        requestObj['headers'] = headers;
    }
    return new Promise((resolve, reject) => {
        console.log('headers = ', headers);
        // resolve(postData);
        request(requestObj, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    // var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(body);
                }
            }
        );
    });
}

async function crawlAreaKorea() {
    while(true) {
        console.log('crawlAreaKorea Loop 시작');
        // await crawlAsianaAreaKorea();
        await crawlKoreanAreaKorea();

        await myTimeout(10*60*1000);
    }
}


async function crawlKoreanAir() {
    if(!dbConnectCompleted) {
        await myTimeout(3000).then(() => {
            console.log('setTimeout completed');
        });
    }
    while(true) {
        console.log('crawlKorean Loop 시작');
        for(let departCode in FLIGHT_INFO_KOREAN) {
            // if(departCode != 'PUS') continue; //테스트로 부산만
            let fromCity = getCityInfo(departCode, AIRPORT_INFO_KOREAN);
            for(var areaKey in FLIGHT_INFO_KOREAN[departCode]) {
                console.log('areaKey = ', areaKey);
                await crawlKoreanArea(areaKey, fromCity);
            }
        }
        
        // for(var areaKey in AIRPORT_INFO_KOREAN) {
        //     if(areaKey != 'Korea') {
        //         console.log('areaKey = ' + areaKey);
        //         await crawlKoreanArea(areaKey);
        //     }
        // }

        await myTimeout(10*60*1000);
    }
    // var toArea = 'EA';
    // var toCityInfo = {
    //     airport : 'DAD',
    //     city : 'DAD',
    //     cityName : '다낭'
    // }
    // crawlKoreanCity(toArea, toCityInfo);
}

//2019-02-18  -> [['2019-02', '2019-02-18], ['2019-03', '2019-03-01]...]
function getMonthArray(stdDateStr, limitDateStr) {
    let year = parseInt(stdDateStr.substring(0, 4));
    let month = parseInt(stdDateStr.substring(5, 7));
    var retObj = [];
    for (let i = 0; i < 13; i++) {
        let newMonth = (month + i - 1)%12 + 1;
        let monthStr = newMonth<10? '0'+newMonth : ''+newMonth;
        let dayStr = '01';
        let newYear = year;
        if(month + i > 12) {
            newYear++;
        }
        if(i == 0) {
            let day = parseInt(stdDateStr.substring(8, 10));
            dayStr = (day < 10?'0':'') + day;
        }
        if(dayStr <= limitDateStr) {
            retObj.push([newYear + '-' + monthStr, newYear + '-' + monthStr + '-' + dayStr]);
        }
    }
    return retObj;
}

function getCityInfo(cityCode, airportInfo) {
    for(let area in airportInfo) {
        let cities = airportInfo[area];
        for(let cityIdx = 0; cityIdx < cities.length; cityIdx++) {
            if(cities[cityIdx].airport == cityCode) {
                return cities[cityIdx];
            }
        }
    }
    return null;
}

async function testCrawlKoreanArea(targetCity) {
    let result = false;
    let toArea, cityIdx;
    for(let area in AIRPORT_INFO_KOREAN) {
        let cities = AIRPORT_INFO_KOREAN[area];
        for (let i = 0; i < cities.length; i++) {
            if(cities[i].airport == targetCity) {
                toArea = area;
                cityIdx = i;
                break;
            }
        }
    }
    console.log('testCrawlKoreanArea targetCity = ' + targetCity);
    if(toArea) {
        let cities = AIRPORT_INFO_KOREAN[toArea];
        let retryCount = 3;
        let curEnvironment;
        while(!result && retryCount-- > 0) {
            console.log('대한항공 ' + cities[cityIdx].cityName + '(' + cities[cityIdx].airport + ') 탐색 시작!!!');
            let resultObj = await crawlKoreanCity(toArea, cities[cityIdx], curEnvironment);
            result = resultObj.result;
            curEnvironment = resultObj.environment;
        }
    }
}

async function crawlKoreanArea(toArea, fromCity) {
    var flightInfos = FLIGHT_INFO_KOREAN[fromCity.airport][toArea];
    // console.log('fromCity = ', fromCity);
    // console.log('flightInfos = ', flightInfos);
    for(let i = 0; i < flightInfos.length; i++) {
        var diffUpdatedHour = 100;
        var dateChanged = true;
        let cityCode = flightInfos[i].airport;
        let updatedTime = await getUpdatedTime('KOREAN', fromCity.airport, flightInfos[i].airport);
        console.log('updatedTime = ', updatedTime);
        // console.log('updatedTime = %i-%i, %i:%i:%i', updatedTime.getMonth(), updatedTime.getDate(), updatedTime.getHours(), updatedTime.getMinutes(), updatedTime.getSeconds());
        var curDate = new Date();
        // console.log('curDate = ', curDate);
        // console.log('curDate = %i-%i, %i:%i:%i', curDate.getMonth(), curDate.getDate(), curDate.getHours(), curDate.getMinutes(), curDate.getSeconds());
        if(updatedTime) {
            var diffUpdated = curDate - updatedTime;
            var hourUnit = 1000*60*60;
            diffUpdatedHour = parseInt(diffUpdated/hourUnit);
            dateChanged = (curDate.getDate() != updatedTime.getDate());
            console.log('diffUpdatedHour = %i, curD = %i, updateD = %i dateChanged = ' + dateChanged, diffUpdatedHour, curDate.getDate(), updatedTime.getDate());
        } else {
            console.log('diffUpdatedHour = %i, curD = %i', diffUpdatedHour, curDate.getDate());
        }
        if(diffUpdatedHour >= 23 || dateChanged) {
            let result = false;
            let retryCount = 3;
            let curEnvironment;
            let toCity = getCityInfo(cityCode, AIRPORT_INFO_KOREAN);
            while(!result && retryCount-- > 0) {
                console.log('대한항공 ' + fromCity.airport + ' > ' + toCity.airport + ' 탐색 시작!!!');
                let resultObj = await crawlKoreanCity(toArea, toCity, curEnvironment, fromCity);
                result = resultObj.result;
                curEnvironment = resultObj.environment;
            }
        } else {
            console.log('대한항공 ' + fromCity.airport + ' > ' + cityCode + ' 업데이트 존재!!!');
        }
    }
    // var cities = AIRPORT_INFO_KOREAN[toArea];
    // for (let i = 0; i < cities.length; i++) {
    //     var diffUpdatedHour = 100;
    //     var dateChanged = true;
    //     if(cities[i].isGroup) continue;
    //     console.log('city = ' + cities[i].cityName);
    //     let updatedTime = await getUpdatedTime('KOREAN', fromCity.airport, cities[i].airport);
    //     console.log('updatedTime = ', updatedTime);
    //     if(updatedTime) {
    //         var curDate = new Date();

    //         var diffUpdated = curDate - updatedTime;
    //         var hourUnit = 1000*60*60;
    //         diffUpdatedHour = parseInt(diffUpdated/hourUnit);
    //         dateChanged = (curDate.getDate() != updatedTime.getDate());
    //         console.log('diffUpdatedHour = %i, curD = %i, updateD = %i dateChanged = ' + dateChanged, diffUpdatedHour, curDate.getDate(), updatedTime.getDate());
    //     } else {
    //         console.log('diffUpdatedHour = %i, curD = %i', diffUpdatedHour, curDate.getDate());
    //     }
    //     if(diffUpdatedHour >= 23 || dateChanged) {
    //         let result = false;
    //         let retryCount = 3;
    //         let curEnvironment;
    //         while(!result && retryCount-- > 0) {
    //             console.log('대한항공 ' + cities[i].cityName + '(' + cities[i].airport + ') 탐색 시작!!!');
    //             let resultObj = await crawlKoreanCity(toArea, cities[i], curEnvironment, fromCity);
    //             result = resultObj.result;
    //             curEnvironment = resultObj.environment;
    //         }
    //     } else {
    //         console.log('24시간 이내 업데이트 존재!!!');
    //     }
    // }
}




async function crawlKoreanCity(toArea, toCityInfo, prevEnvironment, fromCityInfo) {    
    const SEAT_TYPE_CODE = ['X', 'O', 'A']; //시트 코드, 이코노미 X, 비지니스 O, 일등석 A
    const SEAT_TYPE_NAME = ['ECONOMY', 'PRESTIGE', 'FIRST'];
    var dbWriteCompleted = false;
    var environment = {};
    if (prevEnvironment) {
        environment = prevEnvironment;
    } else {
        environment['availableSeatCompleted'] = false;
        environment['availableSeat'] = [{}, {}];
        environment['remainSeat'] = [{}, {}];
        environment['searchStartDate'] = new Date();
        environment['searchStartDateStr'] = dateToYYYYMMDDWith(environment['searchStartDate']);
        environment['curDepartureDate'] = new Date();
        environment['curDepartureDate'].setTime(environment['searchStartDate'].getTime());
        environment['curArrivalDate'] = new Date();
        environment['curArrivalDate'].setTime(environment['searchStartDate'].getTime());
        environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
        // environment['funcYYYYMMDD'] = dateToYYYYMMDD;
        // environment['funcYYYYMMDDWith'] = dateToYYYYMMDDWith;
        environment['curDepartureDateStr'] = dateToYYYYMMDDWith(environment['curDepartureDate']);
        environment['curArrivalDateStr'] = dateToYYYYMMDDWith(environment['curArrivalDate']);
        environment['departureAreaR'] = 'KR';
        if(fromCityInfo) {
            environment['departureAirportR'] = fromCityInfo.airport;
            environment['departureCityNameR'] = fromCityInfo.cityName;
            environment['departureCityR'] = fromCityInfo.cityName;
        } else {
            environment['departureAirportR'] = 'ICN';
            environment['departureCityNameR'] = '서울';
            environment['departureCityR'] = '서울';
        }
        environment['arrivalAirportR'] = toCityInfo.airport;
        environment['arrivalCityR'] = toCityInfo.city;
        environment['arrivalAreaR'] = toArea;
        environment['arrivalCityNameR'] = toCityInfo.cityName;

        environment['seatIdx'] = 0;
        environment['typeIdx'] = 0;
    
        environment['finalSearchDate'] = new Date();
        environment['finalSearchDate'].setDate(environment['finalSearchDate'].getDate()+360);
        environment['finalSearchDateStr'] = dateToYYYYMMDDWith(environment['finalSearchDate']);
    
        console.log('finalSearchDateStr = ', environment['finalSearchDateStr']);
    }

    // const browser = await puppeteer.launch({args : [
    // const browser = await puppeteer.launch({headless: false, args : [
    //         '--window-size=1280,960'
    //     ]});
    console.log('curDepartureDate = ', environment['curDepartureDateStr']);
    console.log('curArrivalDate = ', environment['curArrivalDateStr']);
    // const page = await browser.newPage();
    // page.on('response', async response => {
    //     const req = response.request();
    //     // console.log(req.method(), response.status, req.url());
    //     if(req.url().indexOf('monthly') > 0) {
    //         console.log('-------' + req.url() + '---------');
    //         const text = await response.text();
    //         console.log('text = ' + text);
    //         console.log('postData = ', req.postData());
    //     }
    //     if(req.url().indexOf('koreanair.com/api/fly/award/from') > 0) {
    //         console.log('-------' + req.url() + '---------');
    //         timeStamp = new Date();
    //         initTimeStamp = timeStamp.getTime();
    //         console.log('initTimeStamp = ', initTimeStamp);

    //         console.log("HEADERS:" + JSON.stringify([req.headers()]) + "\n");
    //         requestGetHeader = req.headers();
    //         console.log('requestGetHeader = ', requestGetHeader);
    //         const text = await response.text();
    //         console.log('text = ' + text);
    //     }
    //   });
    // page.on('console', msg => {
    //     for (let i = 0; i < msg.args.length; ++i)
    //         console.log(`${i}: ${msg.args[i]}`);
    //     });
    // page.on('dialog', async dialog => {
    //     // console.log(dialog.message());
    //     await dialog.dismiss();
    // });
    // await page.setViewport({
    //     width: 1280,
    //     height: 960
    // })
    try {
        let MAX_EMPTY_CNT = 6;
        if(!environment['availableSeatCompleted']) {
            let monthArray = getMonthArray(environment['curDepartureDateStr'], environment['finalSearchDate']);
            for(let seatIdx = 0; seatIdx < SEAT_TYPE_CODE.length; seatIdx++) {
                let emptyCount = 0;
            // for(let seatIdx = 0; seatIdx < 1; seatIdx++) {
                for(let typeIdx = 0; typeIdx < 2 && emptyCount < MAX_EMPTY_CNT; typeIdx++) {
                // for(let typeIdx = 0; typeIdx < 1; typeIdx++) {
                    let departureCode, arrivalCode;
                    if(typeIdx == 0) {
                        departureCode = environment['departureAirportR'];
                        arrivalCode = environment['arrivalAirportR'];    
                    } else {
                        arrivalCode = environment['departureAirportR'];
                        departureCode = environment['arrivalAirportR'];    
                    }
                    for(let i = 0; i < monthArray.length && emptyCount < MAX_EMPTY_CNT; i++) {
                    // for(let i = 0; i < 3; i++) {
                        console.log('CHECK AVAIL : ' + departureCode + ' > ' + arrivalCode + '(' + monthArray[i][1] + ', ' + SEAT_TYPE_NAME[seatIdx] + ')');
                        let resultRes = await requestUrlPost('https://kr.koreanair.com/api/viewBonusSeat/monthly', '{"departureCityApo":"' + departureCode + '","arrivalCityApo":"' + arrivalCode + '","awardBookingClass":"' + SEAT_TYPE_CODE[seatIdx] + '","desiredTravelDate":"' + monthArray[i][1] + '","desiredTravelMonth":"' + monthArray[i][0] + '","tripType":"outbound"}');
                        let resultObj = resultRes['viewSeatLists'];
                        if(resultObj) {
                            if(resultObj.length > 0) {
                                emptyCount = 0;
                            } else {
                                emptyCount++;
                            }
                            for(let j = 0; j < resultObj.length; j++ ) {
                                let depDateStr = resultObj[j]['departureDate'];
                                // console.log('depDateStr = ', depDateStr);
                                if(!environment['availableSeat'][typeIdx][depDateStr]) {
                                    environment['availableSeat'][typeIdx][depDateStr] = {};
                                    for(let k = 0; k < SEAT_TYPE_CODE.length; k++) {
                                        environment['availableSeat'][typeIdx][depDateStr][SEAT_TYPE_CODE[k]] = false;
                                    }
                                }
                                environment['availableSeat'][typeIdx][depDateStr][SEAT_TYPE_CODE[seatIdx]] = (resultObj[j]['reservationAvailability'] == 'Y');
                                // console.log('1.availableSeat = ', environment['availableSeat'][0]);
                            }
                        }
                        await myTimeout(200);
                    }
                }
            }
            // console.log('availableSeat = ', environment['availableSeat'][0]);
            environment['availableSeatCompleted'] = true;
        }


        for(let seatIdx = environment['seatIdx']; seatIdx < SEAT_TYPE_CODE.length; seatIdx++) {
            environment['seatIdx'] = seatIdx;
            // console.log('seatIdx = ', seatIdx);
            for(let typeIdx = 0; typeIdx < 2; typeIdx++) {
            // for(let typeIdx = 0; typeIdx < 1; typeIdx++) {
                environment['typeIdx'] = typeIdx;
                if(typeIdx == 0) {
                    departureCode = environment['departureAirportR'];
                    arrivalCode = environment['arrivalAirportR'];    
                } else {
                    arrivalCode = environment['departureAirportR'];
                    departureCode = environment['arrivalAirportR'];    
                }
                for(let dateKey in environment['availableSeat'][typeIdx]) {
                    if(dateKey >= environment['curDepartureDateStr']) {
                        environment['curDepartureDateStr'] = dateKey;
                        if(environment['availableSeat'][typeIdx][dateKey][SEAT_TYPE_CODE[seatIdx]]) {
                            let newDateKey = convert2DateMMDDYYYY(dateKey);
                            let curDate = dateKey.replace(/-/gi, '');
                            // console.log(departureCode + ' > ' + arrivalCode + '(' + newDateKey + ', ' + SEAT_TYPE_NAME[seatIdx] + ')');
                            if(!environment['remainSeat'][typeIdx][curDate]) {
                                environment['remainSeat'][typeIdx][curDate] = {};
                            }
                            let monthStr = dateKey.substring(0, 7);
                            let flightRes = await requestUrlPost('https://kr.koreanair.com/api/viewBonusSeat/flight', '{"departureCityApo":"' + departureCode + '","arrivalCityApo":"' + arrivalCode + '","awardBookingClass":"' + SEAT_TYPE_CODE[seatIdx] + '","desiredTravelDate":"' + dateKey + '","desiredTravelMonth":"' + monthStr + '","tripType":"outbound"}');
                            let flightObjs = flightRes['viewSeatLists'];
                            if(flightObjs) {
                                for(let scIdx = 0; scIdx < flightObjs.length; scIdx++) {
                                    if(flightObjs[scIdx]['reservationAvailability'] == 'Y') {
                                        let scheldule = flightObjs[scIdx]['departureTime'];
                                        if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
                                            environment['remainSeat'][typeIdx][curDate][scheldule] = {};
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'] = [0, 0, 0];
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['mileage'] = [0, 0, 0];
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['tax'] = [0, 0, 0];
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['aircraftType'] = flightObjs[scIdx]['flightNumber'];
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['flightTime'] = 0;
                                        }
                                        environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatIdx] = Math.min(9, parseInt(flightObjs[scIdx]['availableSeat']));
                                        console.log(departureCode + ' > ' + arrivalCode + ' ' + curDate + ' ' + scheldule + ' ' + SEAT_TYPE_NAME[seatIdx] + ' ' + parseInt(flightObjs[scIdx]['availableSeat']));
                                    }
                                }
                            }
                            await myTimeout(200);      
                        }
                    }
                }
                //출발날짜 초기화
                environment['curDepartureDateStr'] = environment['searchStartDateStr'];
            }
        }

        //확인 로그
        // let typeIdx = 0;
        // let depCode, arrCode;
        // if(typeIdx == 0) {
        //     depCode = environment['departureAirportR'];
        //     arrCode = environment['arrivalAirportR'];    
        // } else {
        //     depCode = environment['arrivalAirportR'];    
        //     arrCode = environment['departureAirportR'];
        // }
        // for(let sdate in environment['remainSeat'][typeIdx]) {
        //     for(let stime in environment['remainSeat'][typeIdx][sdate]) {
        //         let remainObj = environment['remainSeat'][typeIdx][sdate][stime];
        //         console.log('%s -> %s (%s %s) : ', depCode, arrCode, sdate, stime, remainObj);
        //     }
        // }


        

        //----------------------로그인 한 후 좌석 상세 가져오는 코드 시작 ----------------------------------
        // await page.goto('https://kr.koreanair.com/korea/ko/booking/booking-gate.html?awa#domestic-award');
        // await page.evaluate((environment) => {
        //     document.querySelector('#usernameInput').value = '';
        //     document.querySelector('#passwordInput').value = '';
        // }, environment);

        // await page.click('#modalLoginButton');
        // await page.waitFor(5000);

        // for(let seatIdx = environment['seatIdx']; seatIdx < SEAT_TYPE_CODE.length; seatIdx++) {
        //     environment['seatIdx'] = seatIdx;
        //     // console.log('seatIdx = ', seatIdx);
        //     for(let typeIdx = 0; typeIdx < 2; typeIdx++) {
        //     // for(let typeIdx = 0; typeIdx < 1; typeIdx++) {
        //         environment['typeIdx'] = typeIdx;
        //         if(typeIdx == 0) {
        //             departureCode = environment['departureAirportR'];
        //             arrivalCode = environment['arrivalAirportR'];    
        //         } else {
        //             arrivalCode = environment['departureAirportR'];
        //             departureCode = environment['arrivalAirportR'];    
        //         }
        //         for(let dateKey in environment['availableSeat'][typeIdx]) {
        //             if(dateKey >= environment['curDepartureDateStr']) {
        //                 environment['curDepartureDateStr'] = dateKey;
        //                 if(environment['availableSeat'][typeIdx][dateKey][SEAT_TYPE_CODE[seatIdx]]) {
        //                     let newDateKey = convert2DateMMDDYYYY(dateKey);
        //                     let curDate = dateKey.replace(/-/gi, '');
        //                     console.log(departureCode + ' > ' + arrivalCode + '(' + newDateKey + ', ' + SEAT_TYPE_NAME[seatIdx] + ')');
        //                     if(!environment['remainSeat'][typeIdx][curDate]) {
        //                         environment['remainSeat'][typeIdx][curDate] = {};
        //                     }
        //                     url = "https://kr.koreanair.com/api/fly/award/from/" + departureCode + "/to/" + arrivalCode + "/on/" + newDateKey + "-0000?flexDays=0&scheduleDriven=false&purchaseThirdPerson=false&domestic=false&isUpgradeableCabin=false&adults=1&children=0&infants=0&cabinClass=" + SEAT_TYPE_NAME[seatIdx] + "&adultDiscounts=&adultInboundDiscounts=&childDiscounts=&childInboundDiscounts=&infantDiscounts=&infantInboundDiscounts=&_=" + new Date().getTime();
        //                     // console.log('url = ', url);
        //                     await page.goto(url);
        //                     let content;
        //                     while(!content || content.length < 3) {
        //                         await page.waitFor(500);
        //                         // let content = await page.content();
        //                         content = await page.evaluate((environment) => {
        //                             return document.querySelector('body').textContent;
        //                         });
        //                     }
        //                     let contentObj = JSON.parse(content);
        //                     // console.log('contentObj = ', contentObj);
        //                     if(contentObj.code) {
        //                         console.log('contentObj = ', contentObj);
        //                         if(contentObj.code == '66002' || contentObj.code == '10032') { //좌석 없음
        //                         } else { //
    
        //                         }
        //                     } else {
        //                         let mileage = 0, tax = 0;
        //                         for(let tempKey in contentObj.fares) {
        //                             let faresObj = contentObj.fares[tempKey].fares;
        //                             mileage = faresObj[0].miles;
        //                             tax = faresObj[0].total;
        //                         }
        //                         let outbounds = contentObj.outbound;
        //                         for(let i = 0; i < outbounds.length; i++) {
        //                             let flights = outbounds[i].flights;
        //                             if(flights.length > 1) { //무슨 상황일까~~~~~?
        //                                 console.log('[ERROR] flights.length = ' + flights.length + ' !!!!!!!!!!!!!!!!!!');
        //                             }
        //                             let remainingSeatsByCabinClass = outbounds[i].remainingSeatsByCabinClass;
        //                             let scheldule = outbounds[i].departure.substring(11, 16) + '-' + outbounds[i].arrival.substring(11, 16);
        //                             for(let tempKey in remainingSeatsByCabinClass) {
        //                                 if(tempKey == SEAT_TYPE_NAME[seatIdx] && remainingSeatsByCabinClass[tempKey] > 0) {
        //                                     if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule] = {};
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'] = [0, 0, 0];
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule]['mileage'] = [0, 0, 0];
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule]['tax'] = [0, 0, 0];
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule]['aircraftType'] = flights[0].aircraft;
        //                                         environment['remainSeat'][typeIdx][curDate][scheldule]['flightTime'] = parseInt(flights[0].flightTime)/1000;
        //                                     }
        //                                     environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatIdx] = remainingSeatsByCabinClass[tempKey];
        //                                     environment['remainSeat'][typeIdx][curDate][scheldule]['mileage'][seatIdx] = mileage;
        //                                     environment['remainSeat'][typeIdx][curDate][scheldule]['tax'][seatIdx] = tax;
        //                                 }
        //                                 // console.log('remainSeat = ', environment['remainSeat'][typeIdx]);
        //                             }
        //                         }
                        
        //                     }
        //                 }    
        //             }
        //         }
        //         //출발날짜 초기화
        //         environment['curDepartureDateStr'] = environment['searchStartDateStr'];
        //     }
        // }
        // ---------------------- 로그인 후 상세 가져오기 끝---------------------------------------------

        // console.log('1. remainSeat = ', environment['remainSeat']);
        saveKoreanSeat(environment['departureAirportR'], environment['departureCityNameR'], environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['remainSeat'][0]);
        saveKoreanSeat(environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['departureAirportR'], environment['departureCityNameR'], environment['remainSeat'][1]);
        dbWriteCompleted = true;
    } catch(err) {
        console.log('Exception catch!!!!!');
        console.log('err : ', err);
    }
    // await browser.close();
    return {result: dbWriteCompleted, environment:environment};
}

function saveKoreanSeat(departureId, departureName, arrivalId, arrivalName, seatData) {
    console.log('DB WRITE %s(%s) -> %s(%s)', departureName, departureId, arrivalName, arrivalId);
    // console.log("seatData = ", seatData);
    let seat = new KoreanSeat();
    seat.departureId = departureId;
    seat.departureName = departureName;
    seat.arrivalId = arrivalId;
    seat.arrivalName = arrivalName;
    for (let sdate in seatData) {
        for (let stime in seatData[sdate]) {
            // let flightTimeMinute = parseInt(seatData[sdate][stime]['flightTime']/60);
            // let flightTime = parseInt(flightTimeMinute/60) + '시간 ' + (flightTimeMinute%60) + '분';
            seat.seatState.push({sdate:sdate, stime:stime, aircraftType:seatData[sdate][stime]['aircraftType'], seatNum:seatData[sdate][stime]['seatNum'], mileage:seatData[sdate][stime]['mileage'], tax:seatData[sdate][stime]['tax']});
            // seat.seatState.push({sdate:sdate, stime:stime, aircraftType:seatData[sdate][stime]['aircraftType'], seatNum:seatData[sdate][stime]['seatNum']});
        }
    }
    KoreanSeat.findOneAndUpdate(
        {
            "departureId" : seat.departureId,
            "arrivalId" : seat.arrivalId
        },
        {
            $currentDate: {
                updated: true
            },
            $set : {
                "departureName" : seat.departureName,
                "arrivalName" : seat.arrivalName,
                "seatState" : seat.seatState
            }
        },
        {
           upsert : true,
           new:true
        },
        (err, newSeat) => {
            console.log('err - ', err);
            // console.log('newSeat - ', newSeat);
        }
    );
}

async function crawlAsiana() {
    while(true) {
        console.log('crawlAsiana Loop 시작');
        for(let departCode in FLIGHT_INFO) {
            // if(departCode != 'PUS') continue; //테스트로 부산만
            let fromCity = getCityInfo(departCode, AIRPORT_INFO);
            for(var areaKey in FLIGHT_INFO[departCode]) {
                console.log('areaKey = ', areaKey);
                await crawlAsianaArea(areaKey, fromCity);
            }
        }

        // for(var areaKey in AIRPORT_INFO) {
        //     console.log('areaKey = ' + areaKey);
        //     if(areaKey != 'KR') {
        //         await crawlAsianaArea(areaKey);
        //     }
        // }
        await myTimeout(10*60*1000);
    }
}

async function crawlAsianaArea(toArea, fromCity) {
    var flightInfos = FLIGHT_INFO[fromCity.airport][toArea];

    for(let i = 0; i < flightInfos.length; i++) {
        try {
            var diffUpdatedHour = 100;
            let cityCode = flightInfos[i].airport;
            // if(cityCode != 'HNL') continue;
            console.log('city = ' + flightInfos[i].cityName);
            let updatedTime = await getUpdatedTime('ASIANA', fromCity.airport, flightInfos[i].airport);
            console.log('updatedTime = ', updatedTime);
            if(updatedTime) {
                var curDate = new Date();
                var diffUpdated = curDate - updatedTime;
                var hourUnit = 1000*60*60;
                diffUpdatedHour = parseInt(diffUpdated/hourUnit);
            }
            console.log('diffUpdatedHour = ', diffUpdatedHour);
            if(diffUpdatedHour >= 23) {
                let result = false;
                let retryCount = 3;
                let curEnvironment;
                let toCity = getCityInfo(cityCode, AIRPORT_INFO);
                if(toCity) {
                    while(!result && retryCount-- > 0) {
                        console.log('아시아나 ' + fromCity.airport + ' > ' + toCity.airport + ' 탐색 시작!!!');
                        let searchDateStr = '20190101';
                        if(curEnvironment != null) {
                            searchDateStr = curEnvironment['curDepartureDateStr'];
                        }
                        let resultObj = await crawlAsianaCity(toArea, toCity, curEnvironment, fromCity);
                        result = resultObj.result;
                        curEnvironment = resultObj.environment;
                        if(curEnvironment['curDepartureDateStr'] != searchDateStr) { //서칭이 조금이라도 진행된 경우
                            console.log("curDepartureDateStr = " + curEnvironment['curDepartureDateStr'] + " searchDateStr = " + searchDateStr);
                            retryCount = 3;
                        }
                    }
                } else {
                    console.log('아시아나 ' + cityCode + ' 찾기 오류!!!');
                }
            } else {
                console.log('아시아나 ' + fromCity.airport + ' > ' + cityCode + ' 24시간 이내 업데이트 존재!!!');
            }
        }
        catch(err) {
            console.log('에러발생 crawlAsianaArea : ', err);
        }
    }
}
async function crawlAsianaAreaKorea() {
    var toArea = 'KR';
    var cities = AIRPORT_INFO[toArea];
    var fromCity;
    for (let i = 0; i < cities.length; i++) {
        var diffUpdatedHour = 100;
        if(cities[i].airport == 'ICN') continue;
        if(cities[i].airport == 'GMP') {
            fromCity = cities[i];
            continue;
        }
        console.log('city = ' + cities[i].cityName);
        let updatedTime = await getUpdatedTime('ASIANA', 'ICN', cities[i].airport);
        console.log('updatedTime = ', updatedTime);
        if(updatedTime) {
            var curDate = new Date();
            var diffUpdated = curDate - updatedTime;
            var hourUnit = 1000*60*60;
            diffUpdatedHour = parseInt(diffUpdated/hourUnit);
        }
        console.log('diffUpdatedHour = ', diffUpdatedHour);
        if(diffUpdatedHour >= 23) {
            let result = false;
            let retryCount = 3;
            let curEnvironment;
            while(!result && retryCount-- > 0) {
                console.log('ASIANA ' + cities[i].cityName + '(' + cities[i].airport + ') 탐색 시작!!!');
                let resultObj = await crawlAsianaCity(toArea, cities[i], curEnvironment, fromCity);
                result = resultObj.result;
                curEnvironment = resultObj.environment;
            }
        } else {
            console.log('24시간 이내 업데이트 존재!!!');
        }

    }
}

async function crawlAsianaCity(toArea, toCityInfo, prevEnvironment, fromCityInfo) {
    var dbWriteCompleted = false;
    var environment = {};
    if (prevEnvironment) {
        environment = prevEnvironment;
    } else {
        environment['availableSeat'] = [{}, {}];
        environment['remainSeat'] = [{}, {}];
        environment['curDepartureDate'] = new Date();
        environment['curArrivalDate'] = new Date();
        environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
        // environment['funcYYYYMMDD'] = dateToYYYYMMDD;
        // environment['funcYYYYMMDDWith'] = dateToYYYYMMDDWith;
        environment['curDepartureDateStr'] = dateToYYYYMMDD(environment['curDepartureDate']);
        environment['curArrivalDateStr'] = dateToYYYYMMDD(environment['curArrivalDate']);
        environment['departureAreaR'] = 'KR';
        if(fromCityInfo) {
            environment['departureAirportR'] = fromCityInfo.airport;
            environment['departureCityNameR'] = fromCityInfo.cityName;
            environment['departureCityR'] = fromCityInfo.cityName;
        } else {
            environment['departureAirportR'] = 'ICN';
            environment['departureCityNameR'] = '서울';
            environment['departureCityR'] = '서울';
        }
        environment['arrivalAirportR'] = toCityInfo.airport;
        environment['arrivalCityR'] = toCityInfo.city;
        environment['arrivalAreaR'] = toArea;
        environment['arrivalCityNameR'] = toCityInfo.cityName;
    
        environment['finalSearchDate'] = new Date();
        environment['finalSearchDate'].setDate(environment['finalSearchDate'].getDate()+360);
        environment['finalSearchDateStr'] = dateToYYYYMMDD(environment['finalSearchDate']);
    
        console.log('finalSearchDateStr = ', environment['finalSearchDateStr']);
    }

    const browser = await puppeteer.launch({args : [
    // const browser = await puppeteer.launch({headless: false, args : [
        '--window-size=1280,960'
    ]});
    console.log('curDepartureDate = ', environment['curDepartureDateStr']);
    console.log('curArrivalDate = ', environment['curArrivalDateStr']);
    const page = await browser.newPage();

    // page.on('response', async response => {
    //     const req = response.request();
    //     if(req.url().indexOf('InternationalAvail') > 0) {
    //         console.log('-------' + req.url() + '---------');
    //         const text = await response.text();
    //         console.log('text = ' + text);
    //         console.log('postData = ', req.postData());
    //     }
    //   });

    page.on('console', msg => {
        for (let i = 0; i < msg.args.length; ++i)
            console.log(`${i}: ${msg.args[i]}`);
        });
    page.on('dialog', async dialog => {
        // console.log(dialog.message());
        await dialog.dismiss();
    });
    await page.setViewport({
        width: 1280,
        height: 960
    })
    try {
        await page.goto('https://flyasiana.com/C/KR/KO/index#none');
        await page.click(mileUseButton);
        await page.evaluate((environment) => {
            document.querySelector('#departureAirportR').value = environment['departureAirportR'];
            document.querySelector('#departureCityR').value = environment['departureCityR'];
            document.querySelector('#departureAreaR').value = environment['departureAreaR'];
            document.querySelector('#departureCityNameR').value = environment['departureCityNameR'];

            document.querySelector('#arrivalAirportR').value = environment['arrivalAirportR'];
            document.querySelector('#arrivalCityR').value = environment['arrivalCityR'];
            document.querySelector('#arrivalAreaR').value = environment['arrivalAreaR'];
            document.querySelector('#arrivalCityNameR').value = environment['arrivalCityNameR'];

            document.querySelector('#departureDate1').value = environment['curDepartureDateStr'];
            document.querySelector('#arrivalDate1').value = environment['curArrivalDateStr'];

        }, environment);
        await page.click('input#seat');
        // const DONOT_SEARCH_FIRST_LIST = ['LAX', 'JFK', 'FRA'];
        // if(DONOT_SEARCH_FIRST_LIST.includes(environment['arrivalAirportR'])) {
        //     await page.click('ul.redCabinClass > li:nth-child(2)');
        //     await page.click('ul.redCabinClass > li:nth-child(3)');    
        // } else {
            await page.click('ul.redCabinClass > li:nth-child(1)');
        // }
        await page.click('#registTravel');
        console.log('search Button')
        let openPopupId = 'temp';
        while (openPopupId != null) {
            console.log('3.openPopupId = ', openPopupId);
            await page.waitFor(100);
            openPopupId = await page.evaluate(() => {
                let buttonLayers = document.querySelectorAll('.layer_wrap');
                let openId = null;
                for (let i = 0; i < buttonLayers.length && !openId; i++) {
                    let style = buttonLayers[i].getAttribute('style');
                    let id = buttonLayers[i].getAttribute('id');
                    if (style) {
                        if (style.indexOf('block') > 0) {
                            openId = id;
                        }
                    }
                    console.log('id: %s, style: %s', id, style);
                }
                return Promise.resolve(openId);
            });
            console.log('openPopupId = ', openPopupId);
            console.log('2.openPopupId = ', (openPopupId == null));
            if (openPopupId != null) {
                await page.click('.layer_wrap#' + openPopupId + ' > .layer_pop > .btn_wrap_ceType2 > .red');
            }
            if (openPopupId == 'mileageInfo') {
                console.log('openPopupId change');
                openPopupId = null;
            }
        }
        await page.waitFor(5000);

        let loopCount = 1000;
        let reloadCount = 0;
        while(loopCount-- > 0) {
            let newEnvironment;
            let maxRetryCount = 10;
            console.log('출발 %s - 도착 %s', environment['curDepartureDateStr'], environment['curArrivalDateStr'])
            while((!newEnvironment || !newEnvironment.completed) && --maxRetryCount > 0) {
                await page.waitFor(1000);
                console.log('maxRetryCount = ', maxRetryCount);
                newEnvironment = await page.evaluate((environment) => {
                    environment.completed = false;
                    let bottomDateDivs = document.querySelectorAll('.inner_bottom');
                    if(!bottomDateDivs || bottomDateDivs.length < 2) return environment;
                    // console.log('bottomDateDivs = ', bottomDateDivs);
                    let searchDate1Str = bottomDateDivs[0].textContent;
                    let searchDate2Str = bottomDateDivs[1].textContent;
                    // console.log('searchDate1Str = ', searchDate1Str);
                    // console.log('searchDate2Str = ', searchDate2Str);
                    let searchDate1 = searchDate1Str.substring(searchDate1Str.indexOf('20')).replace(/ /gi, '').replace(/\./gi, '').substring(0, 8);
                    let searchDate2 = searchDate2Str.substring(searchDate1Str.indexOf('20')).replace(/ /gi, '').replace(/\./gi, '').substring(0, 8);
                    // console.log('searchDate1 = ', searchDate1);
                    // console.log('searchDate2 = ', searchDate2);
                    // console.log('curDepartureDateStr = ', environment['curDepartureDateStr']);
                    // console.log('curArrivalDateStr = ', environment['curArrivalDateStr']);
                    // console.log('searchDate1 != curDepartureDateStr', (searchDate1 != environment['curDepartureDateStr']));
                    // console.log('searchDate2 != curArrivalDateStr', (searchDate2 != environment['curArrivalDateStr']));
                    if(searchDate1 != environment['curDepartureDateStr'] || searchDate2 != environment['curArrivalDateStr']) {
                        return environment;
                    }

                    for(let typeIdx = 0; typeIdx < 2; typeIdx++) {
                        let isDomestic = false;
                        let curDate = environment['curDepartureDateStr'];
                        if(typeIdx > 0) {
                            curDate = environment['curArrivalDateStr'];
                        }
                        let areaDivKey = '#avail_Area' + (typeIdx+1);
                        let areaDivKeyDom;
                        if(typeIdx == 0) {
                            areaDivKeyDom = '#depAvail_Area';
                        } else {
                            areaDivKeyDom = '#arrAvail_Area';
                        }
                        // console.log('areaDivKey = ', areaDivKey);
                        var area1 = document.querySelector(areaDivKey);
                        if(!area1) {
                            isDomestic = true;
                            area1 = document.querySelector(areaDivKeyDom);
                        }
                        // console.log('area1 = ', area1);
                        if(!area1) return environment;
                        var calendarSkels = area1.querySelectorAll('.fc-content-skeleton');
                        calendarSkels.forEach((weekItem) => {
                            let dayDetails = weekItem.querySelectorAll('td.fc-day-top');
                            dayDetails.forEach((dayItem) => {
                                let dateStr = dayItem.getAttribute('data-date');
                                // console.log(dateStr);
                                dateStr = dateStr.substring(0, 4) + dateStr.substring(5, 7) + dateStr.substring(8, 10);
                                let ahref = dayItem.querySelector('a');
                                // console.log(dateStr + ' : ' + ahref);
                                let available = ahref?true:false;
                                environment['availableSeat'][typeIdx][dateStr] = available;
                                // if(!available && !environment['remainSeat'][typeIdx][dateStr] && dateStr > curDate) {
                                //     environment['remainSeat'][typeIdx][dateStr] = {};
                                //     environment['remainSeat'][typeIdx][dateStr]['UNKNOWN'] = [0, 0, 0];
                                // }
                            })
                        });
                        // console.log('1.resultVal = ', environment['availableSeat']);
            
                        var scrollUpsell = area1.querySelector('.scroll_upsell');
                        if(isDomestic) {
                            scrollUpsell = document.querySelector('#depAvail_Area');;
                        }
                        // console.log('scrollUpsell = ', scrollUpsell);
                        if(!scrollUpsell) {
                            var airlineEmpty = area1.querySelector('.airline_empty');
                            // console.log('airlineEmpty = ', airlineEmpty);
                            if(!airlineEmpty) {
                                return null;
                            }
                        }
                        // console.log('isDomestic = ', isDomestic);
                        if(scrollUpsell) {
                            if(isDomestic) {
                                var firstInput = scrollUpsell.querySelector('input#jaAvailDataList');
                                // console.log('firstInput = ', firstInput);
                                if(!firstInput) return null;
                                let flightsInfo = JSON.parse(firstInput.getAttribute('value'));
                                let curFlightsInfo = flightsInfo[typeIdx];
                                for(let i = 0; i < curFlightsInfo.length; i++) {
                                    let flightObj = curFlightsInfo[i];
                                    if(!flightObj.soldOut) {
                                        let flightInfoData = flightObj.flightInfoDatas[0];
                                        let aircraftType = flightInfoData.aircraftType;
                                        let leadTime = flightInfoData.flyingTime;
                                        let dtime = flightObj.departureDate.substring(8, 10) + ':' + flightObj.departureDate.substring(10, 12);
                                        let atime = flightObj.arrivalDate.substring(8, 10) + ':' + flightObj.departureDate.substring(10, 12);
                                        let scheldule = dtime + '-' + atime;
                                        let totalSeatNum = 0;
                                        if(!environment['remainSeat'][typeIdx][curDate]) {
                                            environment['remainSeat'][typeIdx][curDate] = {};
                                        }
                                        if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
                                            environment['remainSeat'][typeIdx][curDate][scheldule] = {
                                                aircraftType : aircraftType,
                                                flightTime : leadTime,
                                                mileage : [0, 0, 0],
                                                tax : [0, 0, 0],
                                                seatNum : [0, 0, 0]
                                            };
                                        }
                                        let detailSeatObj = flightObj.commercialFareFamilyDatas;
                                        if(detailSeatObj) {
                                            for(let detailIdx = 0; detailIdx < detailSeatObj.length; detailIdx++) {
                                                let seatGrade = detailSeatObj[detailIdx].commercialFareFamily;
                                                let familyData = detailSeatObj[detailIdx].fareFamilyDatas[0];
                                                let seatGradeIdx;
                                                if (seatGrade == 'ECOBONUS') seatGradeIdx = 0;
                                                else if (seatGrade == 'BIZBONUS') seatGradeIdx = 1;
                                                else seatGradeIdx = 2;
                                                // console.log('seatGradeIdx = ', seatGradeIdx);
                                                environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatGradeIdx] = parseInt(familyData.seatCount);
                                                environment['remainSeat'][typeIdx][curDate][scheldule]['mileage'][seatGradeIdx] = parseInt(familyData.paxTypeFareDatas[0].mileage);
                                                environment['remainSeat'][typeIdx][curDate][scheldule]['tax'][seatGradeIdx] = parseInt(familyData.paxTypeFareDatas[0].totalTax);
                                                totalSeatNum += environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatGradeIdx];
                                            }
                                        }
                                        if(totalSeatNum == 0) {
                                            delete environment['remainSeat'][typeIdx][curDate][scheldule];
                                        }    
                                    }
                                }
                            } else {
                                var flights = scrollUpsell.querySelectorAll('tr.flight');
                                flights.forEach((flight) => {
                                    let flightObj = JSON.parse(flight.getAttribute('data-flightlist'));
                                    console.log('flightObj : ', flightObj);
                                    let aircraftType = flightObj.flightInfoDatas[0].aircraftType;
                                    let leadTime = flightObj.flightInfoDatas[0].flyingTime;
                                    let flightTime = flight.querySelector('.flight_time');
                                    // console.log('flightTime = ', flightTime);
                                    let datimes = flightTime.querySelectorAll('.time');
                                    // console.log('1.datime = <', datimes[0].textContent + ' ~ ' + datimes[1].textContent + '>');
                                    let scheldule = datimes[0].textContent + '-' + datimes[1].textContent;
                                    let totalSeatNum = 0;
                                    if(!environment['remainSeat'][typeIdx][curDate]) {
                                        environment['remainSeat'][typeIdx][curDate] = {};
                                    }
                                    if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
                                        environment['remainSeat'][typeIdx][curDate][scheldule] = {
                                            aircraftType : aircraftType,
                                            flightTime : leadTime,
                                            mileage : [0, 0, 0],
                                            tax : [0, 0, 0],
                                            seatNum : [0, 0, 0]
                                        };
                                    }
                                    let detailSeatObj = flightObj.commercialFareFamilyDatas;
                                    if(detailSeatObj) {
                                        for(let detailIdx = 0; detailIdx < detailSeatObj.length; detailIdx++) {
                                            let seatGrade = detailSeatObj[detailIdx].commercialFareFamily;
                                            let familyData = detailSeatObj[detailIdx].fareFamilyDatas[0];
                                            let seatGradeIdx;
                                            if (seatGrade == 'ECOBONUS') seatGradeIdx = 0;
                                            else if (seatGrade == 'BIZBONUS') seatGradeIdx = 1;
                                            else seatGradeIdx = 2;
                                            // console.log('seatGradeIdx = ', seatGradeIdx);
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatGradeIdx] = parseInt(familyData.seatCount);
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['mileage'][seatGradeIdx] = parseInt(familyData.paxTypeFareDatas[0].mileage);
                                            environment['remainSeat'][typeIdx][curDate][scheldule]['tax'][seatGradeIdx] = parseInt(familyData.paxTypeFareDatas[0].totalTax);
                                            totalSeatNum += environment['remainSeat'][typeIdx][curDate][scheldule]['seatNum'][seatGradeIdx];
                                        }
                                    }
                                    if(totalSeatNum == 0) {
                                        delete environment['remainSeat'][typeIdx][curDate][scheldule];
                                    }
                                });
                            }
                        }                    
                    }
                    // console.log('2.resultVal = ', environment['availableSeat']);
                    // console.log('2.environment = ', environment);
                    environment.completed = true;
                    return environment;
                }, environment);
            }
            if(!newEnvironment.completed) {
                await page.click('input#sCalendar1');

                reloadCount++;
                if(reloadCount > 5) {
                    throw 'reloadCount exceed limit - ' + reloadCount;
                } else if(reloadCount > 3) {
                    environment['dontSearchFirst'] = true;
                }
                console.log('페이지가 이상하여 리로드!!!! --- ' + reloadCount);
                await page.reload();
                await page.waitFor(5000);
                console.log('리로드 대기 완료!!');
            } else {
                reloadCount = 0;
            }
            if(newEnvironment.completed) {
                environment['availableSeat'] = newEnvironment['availableSeat'];
                environment['remainSeat'] = newEnvironment['remainSeat'];
                // console.log('3.environment = ', environment);
                // console.log('3.remainSeat[0] = ', environment['remainSeat'][0]);
                // for(let sdate in environment['remainSeat'][0]) {
                //     for(let stime in environment['remainSeat'][0][sdate]) {
                //         console.log('3.seatData = ', environment['remainSeat'][0][sdate][stime]);
                //     }
                // }
                environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
                let searchCount = 30;
                while(--searchCount > 0) {
                    // console.log('3.curArrivalDate = ', environment['curArrivalDate']);
                    let dateWith = dateToYYYYMMDD(environment['curArrivalDate']);
                    if(environment['availableSeat'][1][dateWith] !== false) break;
                    environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
                }
                if (environment['curArrivalDate'].getTime() > environment['finalSearchDate'].getTime()) {
                    environment['curArrivalDate'].setTime(environment['finalSearchDate'].getTime());
                }
                environment['curArrivalDateStr'] = dateToYYYYMMDD(environment['curArrivalDate']);

                environment['curDepartureDate'].setDate(environment['curDepartureDate'].getDate()+1);
                searchCount = 30;
                while(--searchCount > 0) {
                    // console.log('3.curDepartureDate = ', environment['curDepartureDate']);
                    let dateWith = dateToYYYYMMDD(environment['curDepartureDate']);
                    if(environment['availableSeat'][0][dateWith] !== false) break;
                    if(dateWith == environment['curArrivalDateStr']) break;
                    environment['curDepartureDate'].setDate(environment['curDepartureDate'].getDate()+1);
                }
                if (environment['curDepartureDate'].getTime() > environment['finalSearchDate'].getTime()) {
                    environment['curDepartureDate'].setTime(environment['finalSearchDate'].getTime());
                }
                environment['curDepartureDateStr'] = dateToYYYYMMDD(environment['curDepartureDate']);        
            }

            if(environment['curDepartureDate'].getTime() == environment['finalSearchDate'].getTime() && environment['curArrivalDate'].getTime() == environment['finalSearchDate'].getTime()) break;
        
            await page.evaluate((environment) => {
                document.querySelector('#departureDate1').value = environment['curDepartureDateStr'];
                document.querySelector('#arrivalDate1').value = environment['curArrivalDateStr'];
                if(environment['dontSearchFirst'] == true) {
                    let layerSeat = document.querySelector('.shadow_layer.layer_seat');
                    if(layerSeat) {
                        let lis = layerSeat.querySelectorAll('li');
                        if(lis.length >= 4) {
                            lis[0].setAttribute('class', '');
                            lis[1].setAttribute('class', 'on');
                            lis[2].setAttribute('class', 'on');
                            lis[3].setAttribute('class', '');
                        }
                    }
                }
        
            }, environment);
            if(!newEnvironment.completed) {
                console.log('검색버튼 클릭!!!');
            }
            await page.click('.btn_trv_edit');    
        }

        // for(let typeIdx = 0; typeIdx < 2; typeIdx++) {
        //     let wording = '출발';
        //     if (typeIdx == 1) {
        //         wording = '도착';
        //     }
        //     for(let key in environment['availableSeat'][typeIdx]) {
        //         if (environment['availableSeat'][typeIdx][key] === true) {
        //             console.log(key + ' ' + wording + ' 자리 있음');
        //         }
        //     }
        //     for (let key in environment['remainSeat'][typeIdx]) {
        //         for (let key2 in environment['remainSeat'][typeIdx][key]) {
        //             for (let grade = 0; grade < 3; grade++) {
        //                 if (environment['remainSeat'][typeIdx][key][key2][grade] > 0) {
        //                     console.log(wording + ' ' + key + '(' + key2 + '), grade ' + grade + ' : 좌석 ' + environment['remainSeat'][typeIdx][key][key2][grade]);
        //                 }
        //             }
        //         }
        //     }
        // }
        saveAsianaSeat(environment['departureAirportR'], environment['departureCityNameR'], environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['remainSeat'][0]);
        saveAsianaSeat(environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['departureAirportR'], environment['departureCityNameR'], environment['remainSeat'][1]);
        dbWriteCompleted = true;
        // await page.waitForNavigation();
        // await page.screenshot({path: 'example.png'});
    } catch(err) {
        console.log('Exception catch!!!!!');
        console.log(err);
    }
    await browser.close();
    return {result: dbWriteCompleted, environment:environment};
}



function saveAsianaSeat(departureId, departureName, arrivalId, arrivalName, seatData) {
    console.log("departureId = ", departureId);
    console.log("departureName = ", departureName);
    console.log("arrivalId = ", arrivalId);
    console.log("arrivalName = ", arrivalName);
    // console.log("seatData = ", seatData);
    let seat = new AsianaSeat();
    seat.departureId = departureId;
    seat.departureName = departureName;
    seat.arrivalId = arrivalId;
    seat.arrivalName = arrivalName;
    for (let sdate in seatData) {
        for (let stime in seatData[sdate]) {
            let flightTime = seatData[sdate][stime]['flightTime'].replace(':', '시간 ') + '분';
            seat.seatState.push({sdate:sdate, stime:stime, aircraftType:seatData[sdate][stime]['aircraftType'], flightTime:flightTime, seatNum:seatData[sdate][stime]['seatNum'], mileage:seatData[sdate][stime]['mileage'], tax:seatData[sdate][stime]['tax']});
        }
    }
    AsianaSeat.findOneAndUpdate(
        {
            "departureId" : seat.departureId,
            "arrivalId" : seat.arrivalId
        },
        {
            $currentDate: {
                updated: true
            },
            $set : {
                "departureName" : seat.departureName,
                "arrivalName" : seat.arrivalName,
                "seatState" : seat.seatState
            }
        },
        {
           upsert : true,
           new:true
        },
        (err, newSeat) => {
            console.log('err - ', err);
            // console.log('newSeat - ', newSeat);
        }
    );
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// 현재 안 쓰는 것
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
async function crawlKoreanAreaKorea() {
    var toArea = 'Korea';
    var cities = AIRPORT_INFO_KOREAN[toArea];
    var fromCity;
    for (let i = 0; i < cities.length; i++) {
        var diffUpdatedHour = 100;
        var dateChanged = true;
        if(cities[i].isGroup) continue;
        if(cities[i].airport == 'ICN') continue;
        if(cities[i].airport == 'GMP') {
            fromCity = cities[i];
            continue;
        }
        console.log('city = ' + cities[i].cityName);
        let updatedTime = await getUpdatedTime('KOREAN', 'GMP', cities[i].airport);
        console.log('updatedTime = ', updatedTime);
        var curDate = new Date();
        if(updatedTime) {
            var diffUpdated = curDate - updatedTime;
            var hourUnit = 1000*60*60;
            diffUpdatedHour = parseInt(diffUpdated/hourUnit);
            dateChanged = (curDate.getDate() != updatedTime.getDate());
            console.log('diffUpdatedHour = %i, curD = %i, updateD = %i dateChanged = ' + dateChanged, diffUpdatedHour, curDate.getDate(), updatedTime.getDate());
        } else {
            console.log('diffUpdatedHour = %i, curD = %i', diffUpdatedHour, curDate.getDate());
        }
        if(diffUpdatedHour >= 23 || dateChanged) {
            let result = false;
            let retryCount = 3;
            let curEnvironment;
            while(!result && retryCount-- > 0) {
                console.log('대한항공 ' + cities[i].cityName + '(' + cities[i].airport + ') 탐색 시작!!!');
                let resultObj = await crawlKoreanCity(toArea, cities[i], curEnvironment, fromCity);
                result = resultObj.result;
                curEnvironment = resultObj.environment;
            }
        } else {
            console.log('24시간 이내 업데이트 존재!!!');
        }
    }
}


// console.log('1.zoomCityMatching = ', zoomCityMatching);
function addZoomDataObj(depCode, arrCode, sdate, stime, aircraftType, seatNum) {
    if(!zoomDataObj[depCode]) {
        zoomDataObj[depCode] = {};
    }
    if(!zoomDataObj[depCode][arrCode]) {
        zoomDataObj[depCode][arrCode] = {};
    }
    if(!zoomDataObj[depCode][arrCode][sdate]) {
        zoomDataObj[depCode][arrCode][sdate] = {};
    }
    if(!zoomDataObj[depCode][arrCode][sdate][stime]) {
        zoomDataObj[depCode][arrCode][sdate][stime] = {
            aircraftType : aircraftType,
            seatNum : seatNum
        };
    }
}
const ZOOM_AREA_LIST = [
    {
        area : '일본',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-japan/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_1.xlsx'
    },
    {
        area : '동북아',
        url : 'http://http://zoommytrip.kr/koreanair-mile-arr-china/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_2.xlsx'
    },
    {
        area : '동남아',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-asia/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_3.xlsx'
    },
    {
        area : '서아시아',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-east-asia/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_7.xlsx'
    },
    {
        area : '유럽',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-europe/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_4.xlsx'
    },
    {
        area : '미주',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-usa/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_5.xlsx'
    },
    {
        area : '대양주',
        url : 'http://zoommytrip.kr/koreanair-mile-arr-oc/',
        fileURL : 'http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_6.xlsx'
    }
];


function getCityObjByZoom(zoomcity) {
    // console.log('zoomCityMatching = ', zoomCityMatching);
    let retCityObj = zoomCityMatching[zoomcity];
    if(!retCityObj) {
        for(let area in AIRPORT_INFO_KOREAN) {
            for(let cityIdx = 0; cityIdx < AIRPORT_INFO_KOREAN[area].length && !retCityObj; cityIdx++) {
                let cityObj = AIRPORT_INFO_KOREAN[area][cityIdx];
                if(!cityObj.isGroup) {
                    if(cityObj.cityName.indexOf(zoomcity) >= 0) {
                        retCityObj = cityObj;
                        zoomCityMatching[zoomcity] = cityObj;
                    }
                }
            }
            if(retCityObj) break;
        }
    }
    return retCityObj;
}

function getZoomFileName(zoomInfo) {
    return 'zoom_' + zoomInfo.area + ".xlsx";
}

async function crawlKoreanCityAvail(depCode, arrCode) {
    const SEAT_TYPE_CODE = ['X', 'O', 'A']; //시트 코드, 이코노미 X, 비지니스 O, 일등석 A
    const SEAT_TYPE_NAME = ['ECONOMY', 'PRESTIGE', 'FIRST'];
    var environment = {};
    environment['searchStartDate'] = new Date();
    environment['searchStartDateStr'] = dateToYYYYMMDDWith(environment['searchStartDate']);
    environment['curDepartureDate'] = new Date();
    environment['curDepartureDate'].setTime(environment['searchStartDate'].getTime());
    environment['curArrivalDate'] = new Date();
    environment['curArrivalDate'].setTime(environment['searchStartDate'].getTime());
    environment['finalSearchDate'] = new Date();
    environment['finalSearchDate'].setTime(environment['searchStartDate'].getTime());
    environment['finalSearchDate'].setDate(environment['finalSearchDate'].getDate()+360);
    environment['finalSearchDateStr'] = dateToYYYYMMDDWith(environment['finalSearchDate']);

    let monthArray = getMonthArray(environment['searchStartDateStr'], environment['finalSearchDateStr']);

    environment['availableSeat'] = {};
    for(let seatIdx = 0; seatIdx < SEAT_TYPE_CODE.length; seatIdx++) {
        // for(let i = 0; i < monthArray.length; i++) {
        for(let i = 0; i < 1; i++) {
            let resultRes = await requestUrlPost('https://kr.koreanair.com/api/viewBonusSeat/monthly', '{"departureCityApo":"' + depCode + '","arrivalCityApo":"' + arrCode + '","awardBookingClass":"' + SEAT_TYPE_CODE[seatIdx] + '","desiredTravelDate":"' + monthArray[i][1] + '","desiredTravelMonth":"' + monthArray[i][0] + '","tripType":"outbound"}');
            let resultObj = resultRes['viewSeatLists'];
            if(resultObj) {
                for(let j = 0; j < resultObj.length; j++ ) {
                    let depDateStr = resultObj[j]['departureDate'];
                    // console.log('depDateStr = ', depDateStr);
                    if(!environment['availableSeat'][depDateStr]) {
                        environment['availableSeat'][depDateStr] = [false, false, false];
                    }
                    if(resultObj[j]['reservationAvailability'] == 'Y') {
                        
                    }
                    // console.log('1.availableSeat = ', environment['availableSeat'][0]);
                }
            }
            await myTimeout(500);
        }
    }
    return environment['availableSeat'];
}

async function crawlZoomKorean() {
    // let dataHtml = await requestUrlGet('http://zoommytrip.kr/koreanair-mile-arr-japan/');
    // var $ = cheerio.load(dataHtml);
    // var xlsxLink = $('a.mtli_attachment');
    // console.log('xlsxLink = ', xlsxLink);
    // var xlsxUrl = xlsxLink.attr('href');
    // console.log('xlsxUrl = ', xlsxUrl);

    // let xlsxFile = await requestUrlGet('http://zoommytrip.kr/wp-content/uploads/KoreanAir_MileSeat_Excel/ZoomMyTrip_kr_KOR_ICN_Grp_1.xlsx', null, true);
    // console.log('xlsxFile = ', xlsxFile.length);
    // console.log('xlsxFile = ', typeof xlsxFile);
    // console.log('xlsxFile = ', xlsxFile);

    // var xlsxData = XLSX.read(xlsxFile);
    while(!dbConnectCompleted) {
        await myTimeout(1000);
        console.log('waiting dbConnectCompleted');
    }
    // for(let zoomListIdx = 0; zoomListIdx < ZOOM_AREA_LIST.length; zoomListIdx++) {
    //     let fileName = getZoomFileName(ZOOM_AREA_LIST[zoomListIdx]);
    // }
    let fileName = 'test.xlsx';
    let fileStat;
    try {
        fileStat = fs.statSync(fileName);
    } catch(err) {
        console.log(err);
    }
    if(fileStat) {
        var lastModified = fileStat.mtime.getTime();
        var xlsxData = XLSX.readFile(fileName);
        var sheet_name_list = xlsxData.SheetNames;
        var xlDataObj = xlsxData.Sheets[sheet_name_list[0]];
        var CELLNAME = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        var TAGNAME = ['dep', 'arr', 'date', 'time_1', 'seat_1', 'time_2', 'seat_2']
        for(let i = 0; i < CELLNAME.length; i++) {
            var cellname = CELLNAME[i] + '1';
            xlDataObj[cellname] = { t: 's', v: TAGNAME[i], r: TAGNAME[i], h: TAGNAME[i], w: TAGNAME[i] };
            cellname = CELLNAME[i] + '2';
        }
        var xlData = XLSX.utils.sheet_to_json(xlDataObj);
        console.log(xlData);
        console.log('xlData.length = ', xlData.length);
        let cityMatching = {};
        for(let i = 0; i < xlData.length; i++) {
            let curDate = '20' + xlData[i].date.substring(1, 9).replace(/-/gi, '');
            let depCity = xlData[i].dep;
            // console.log('zoomCityMatching = ', zoomCityMatching);
            let depCityObj = getCityObjByZoom(depCity);
            // console.log('depCityObj = ', depCityObj);
            if(!depCityObj) continue;
            let arrCity = xlData[i].arr;
            let arrCityObj = getCityObjByZoom(arrCity);
            // console.log('arrCityObj = ', arrCityObj);
            if(!arrCityObj) continue;
            let stimeTemp = xlData[i].time_1;
            if(stimeTemp && stimeTemp.length > 3) {
                let stime = stimeTemp.substring(0, 5);
                let aircraftType = stimeTemp.substring(stimeTemp.indexOf('(') + 1, stimeTemp.indexOf(')'));
                let seatNum = [];
                let seatNumInfo = xlData[i].seat_1;
                let seatNumInfoArr = seatNumInfo.split('/');
                for(let k = 0; k < seatNumInfoArr.length; k++) {
                    seatNum.push(parseInt(seatNumInfoArr[k]));
                }
                addZoomDataObj(depCityObj.airport, arrCityObj.airport, curDate, stime, aircraftType, seatNum);
            }
            stimeTemp = xlData[i].time_2;
            if(stimeTemp && stimeTemp.length > 3) {
                let stime = stimeTemp.substring(0, 5);
                let aircraftType = stimeTemp.substring(stimeTemp.indexOf('(') + 1, stimeTemp.indexOf(')'));
                let seatNum = [];
                let seatNumInfo = xlData[i].seat_2;
                let seatNumInfoArr = seatNumInfo.split('/');
                for(let k = 0; k < seatNumInfoArr.length; k++) {
                    seatNum.push(parseInt(seatNumInfoArr[k]));
                }
                addZoomDataObj(arrCityObj.airport, depCityObj.airport, curDate, stime, aircraftType, seatNum);
            }
        }
    
        for(let depCode in zoomDataObj) {
            let depCityObj = getCityObjByZoom(depCode);
            for(let arrCode in zoomDataObj[depCode]) {
                let arrCityObj = getCityObjByZoom(arrCode);
                let count = 0;
                let availInfo = await crawlKoreanCityAvail(depCode, arrCode);
                for(let availDate in availInfo) {
                    let sdate = availDate.replace(/-/gi, '');
                    let seatNum = [0, 0, 0];
                    if(zoomDataObj[depCode][arrCode][sdate]) {
                        for(let stime in zoomDataObj[depCode][arrCode][sdate]) {
                            // console.log('%s -> %s (%s %s, %s) ', depCode, arrCode, sdate, stime, zoomDataObj[depCode][arrCode][sdate][stime]['aircraftType'], zoomDataObj[depCode][arrCode][sdate][stime]['seatNum'])
                            count++;
                            for(let seatIdx = 0; seatIdx < 3; seatIdx++) {
                                if(!availInfo[availDate][seatIdx] && zoomDataObj[depCode][arrCode][sdate][stime]['seatNum'][seatIdx] > 0) {
                                    console.log('%s -> %s (%s %s) %i : zoom : %i, avail : ' + availInfo[availDate][seatIdx], depCode, arrCode, sdate, stime, seatIdx, zoomDataObj[depCode][arrCode][sdate][stime]['seatNum'][seatIdx]);
                                }
                                seatNum[seatIdx] += zoomDataObj[depCode][arrCode][sdate][stime]['seatNum'][seatIdx];
                            }
                        }
                        for(let seatIdx = 0; seatIdx < 3; seatIdx++) {
                            if(availInfo[availDate][seatIdx] && seatNum[seatIdx] <= 0) { 
                                console.log('%s -> %s (%s) %i : zoom : %i, avail : ' + availInfo[availDate][seatIdx], depCode, arrCode, sdate, seatIdx, seatNum[seatIdx]);
                            }
                        }
                    } else {
                        console.log(sdate + ' is not in zoomData ', availInfo[availDate]);
                    }
                    // if(count > 30) break;
                }
                saveKoreanSeat(depCityObj.airport, depCityObj.cityName, arrCityObj.airport, arrCityObj.cityName, zoomDataObj[depCode][arrCode]);
                // saveKoreanSeat(environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['departureAirportR'], environment['departureCityNameR'], environment['remainSeat'][1]);
                // dbWriteCompleted = true;

            }
        }


    }
    // await page.goto('http://zoommytrip.kr/koreanair-mile-arr-japan/');
}
