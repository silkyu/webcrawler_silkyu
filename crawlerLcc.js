const puppeteer = require('puppeteer');
var mongoose = require("mongoose");
const config = require("./config.js");
var program = require("commander");
var request = require('request');
const LccSeat = require('./models/lccseat')
const util = require('util')
const {dateToYYYYMMDD, dateToYYYYMMDDWith, convert2DateMMDDYYYY, myTimeout, getRandomInt} = require("./util.js")
const {AIRPORT_INFO, AIRPORT_INFO_KOREAN, FLIGHT_INFO, FLIGHT_INFO_KOREAN, AIRPORT_INFO_JINAIR, FLIGHT_INFO_JINAIR} = require("./airportinfo.js")

program
.version('1.0.1')
.option('--crawljeju', 'crawl jeju')
.option('--crawljin', 'crawl jinair')
.parse(process.argv);

mongoose.connect(
    config.mongodbUri(),
    { useNewUrlParser: true }
);
mongoose.Promise = global.Promise;

var dbConnectCompleted = false;
var db = mongoose.connection;
//데이터베이스 접속 확인
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function(callback) {
    console.log("mongo DB connected...");
    dbConnectCompleted = true;
});

if (program.crawljeju) {
    crawlJejuAir();
} else if (program.crawljin) {
    crawlJinAir();
} else {
    testPrint();
}

async function testPrint() {
    // let param = '{"id": 13, "method": "DataService.service", "params": [{"javaClass": "com.jein.framework.connectivity.parameter.RequestParameter", "requestUniqueCode": "PGWHC00001", "requestExecuteType": "BIZ", "DBTransaction": false, "sourceName": null, "sourceExtension": null, "functionName": "DTWBA00022", "panelId": null, "methodType": null, "inParameters": {"javaClass": "java.util.List", "list": [{"javaClass": "com.jein.framework.connectivity.parameter.InParameter", "paramName": "flightSearch", "ioType": "IN", "structureType": "FIELD", "data": {"javaClass": "java.util.List", "list": [{"map": {"flightSearch": {"viewType":"B","fly_type":"1","person1":"2","person2":"2","person3":"0","residentCountry":"KR","currency":"","promotion_cd":"","flySection":[{"departure_cd":"ICN","departure_txt":"서울/인천","arrival_cd":"OKA","arrival_txt":"오키나와","departure_date_cd":"20190521","departure_date_txt":"2019-05-21"},{"departure_cd":"OKA","departure_txt":"오키나와","arrival_cd":"ICN","arrival_txt":"서울/인천","departure_date_cd":"20190525","departure_date_txt":"2019-05-25"}],"recaptchaToken":"03AOLTBLQuFksxRYaewS_vTzkLngFgV_8EgUcyoOxKWN2nSLaM6iFK03sSVySc2kl3B1cQkvsIAekyj6boobzySPo0AzhTh1LVbuP4vRr9tkcM7nBAdPdZWFKC4snKboJrsfDof0IRpXDss3IrALSa7WoNNG2UaJ2AlpQ7_-pdmGJEzGlN45kvgIba5JKtDzXTGreVTW3rOqGwkIiHQbiEUt6r0NcBG2Wje5nR8QOLZsFLQg9QSeQdgQflAGToXzzms01YMn2ZaGD0JN1QyDw_y5eikuoj_yqdgQKrL9LiGo5OveX_oVwucn7MHl4SR93zrQBGwyFKMnNW"}}, "javaClass": "java.util.Map"}]}}]}, "filterParameter": {"javaClass": "java.util.Map", "map": {}}}]}';

    // // console.log('param = ', param);
    // console.log('paramObj = ', JSON.parse(param));
    // let curDate = new Date();
    // console.log('curDate = ', curDate.getTime());
    // let flightRes = await requestUrlPostObj('https://www.eastarjet.com/json/dataService?n$eum='+curDate.getTime(), param);
    // console.log('flightRes = ', util.inspect(flightRes, false, null, true));

    // let param = '{"TRIPTYPE":"RT","segmentDatas":[{"arrivalAirport":"OKA","carrierCode":"RS","departureAirport":"ICN","departureDateTime":"20190626000000","flownSeg":false,"layover":false,"noShow":false,"departureAirportName":"서울/인천(ICN)","arrivalAirportName":"오키나와(OKA)","segmentNo":1,"seatCount":4,"originDateTime":"20190628000000"},{"arrivalAirport":"ICN","carrierCode":"RS","departureAirport":"OKA","departureDateTime":"20190702000000","flownSeg":false,"layover":false,"noShow":false,"departureAirportName":"오키나와(OKA)","arrivalAirportName":"서울/인천(ICN)","segmentNo":2,"seatCount":4,"originDateTime":"20190702000000"}],"passengerDatas":[{"passengerNo":1,"paxType":"ADT"},{"passengerNo":2,"paxType":"ADT"},{"passengerNo":3,"paxType":"CHD"},{"passengerNo":4,"paxType":"CHD"}],"USE_CURRENCY":"KRW","LANGUAGE_CODE":"KO","ROUTETYPE":"I","reservationPageIdx":"2"}';
    // // console.log('paramObj = ', JSON.parse(param));
    // let flightRes = await requestUrlPost4AirSeoul('https://flyairseoul.com/I/KO/searchFlightInternational.do', param);
    // console.log('flightRes = ', util.inspect(flightRes, false, null, true));


    // let param = '{"searchType":"","origin1":"ICN","destination1":"OKA","travelDate1":"20190627","origin2":"OKA","destination2":"ICN","travelDate2":"20190629","origin3":"","destination3":"","travelDate3":"","origin4":"","destination4":"","travelDate4":"","pointOfPurchase":"","adultPaxCount":"1","childPaxCount":"0","infantPaxCount":"0","tripType":"RT","cpnNo":"","promoCode":"","refVal":"JINAIR","refPop":"","refChannel":"","refLang":""}';
    // console.log('paramObj = ', JSON.parse(param));
    // let curDate = new Date();
    // console.log('curDate = ', curDate.getTime());
    // let flightRes = await requestUrlPostObj('https://www.jinair.com/booking/getAirAvailabilityJson?n$eum=83748499398216960', param);
    // console.log('flightRes = ', util.inspect(flightRes, false, null, true));

    // let param = 'groupBookingYN=N&criteriaDate=20190624&period=PN&deptAirportCode=ICN&arriAirportCode=OKA&baseDeptAirportCode=ICN&currency=KRW';
    // let flightRes = await requestUrlGet('https://www.twayair.com/ajax/booking/lowestFare?' + param);
    // console.log('flightRes = ', flightRes);
    // let flightResObj = JSON.parse(flightRes);
    // console.log('flightResObj = ', util.inspect(flightResObj, false, null, true));


    let param = '';
    let flightRes = await requestUrlGet('https://www.jinair.com/booking/index' + param);
    console.log('flightRes = ', flightRes);
    // let flightResObj = JSON.parse(flightRes);
    // console.log('flightResObj = ', util.inspect(flightResObj, false, null, true));

}

function getCityInfo(cityCode, airportInfo) {
    for(let area in airportInfo) {
        let cities = airportInfo[area];
        for(let cityIdx = 0; cityIdx < cities.length; cityIdx++) {
            if(cities[cityIdx].airport == cityCode) {
                return cities[cityIdx];
            }
        }
    }
    return null;
}

function requestUrlPost4AirSeoul(url, postData) {
    return new Promise((resolve, reject) => {
        // console.log('postData = ', postData);
        var postDataObj = JSON.parse(postData);
        console.log('postDataObj = ', postDataObj);
        // resolve(postData);
        request(
            {
                url: url,
                method: 'POST',
                form: {
                    hidBookConditionData : JSON.stringify(postDataObj),
                    gubun : 'd25e48e721d3c4f4a8bb5be51e16d0620eec9111e285cf43df6c6fc3b9b83d60b7634a686b096142aa8c033b21e36c30192fd334dcd44debb30b32dcd9185ee7'
                }
            }, 
            function(error, response, body) {
                console.log('body = ', body);
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(bodyObj);
                }
            }
        );
    });
}

function requestUrlPost4JinAir(url, postDataObj, csrf) {
    return new Promise((resolve, reject) => {
        let header = {
            "X-CSRF-TOKEN" : csrf
        };
        // console.log('postData = ', postData);
        console.log('header = ', header);
        console.log('postDataObj = ', postDataObj);
        // resolve(postData);
        request(
            {
                url: url,
                headers: header,
                method: 'POST',
                // body: JSON.stringify(postDataObj),
                // json: true
                form: postDataObj
            }, 
            function(error, response, body) {
                console.log('body = ', body);
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(bodyObj);
                }
            }
        );
    });
}

function requestUrlPostObj(url, postData) {
    return new Promise((resolve, reject) => {
        // console.log('postData = ', postData);
        var postDataObj = JSON.parse(postData);
        // console.log('postDataObj = ', postDataObj);
        // resolve(postData);
        request(
            {
                url: url,
                method: 'POST',
                form: postDataObj
            }, 
            function(error, response, body) {
                console.log('body = ', body);
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(bodyObj);
                }
            }
        );
    });
}


function requestUrlPost(url, postData) {
    return new Promise((resolve, reject) => {
        // console.log('postData = ', postData);
        // resolve(postData);
        request(
            {
                headers: {
                    'content-type': 'application/json',
                },
                url: url,
                method: 'POST',
                body: postData
            }, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(bodyObj);
                }
            }
        );
    });
}

function requestUrlGet(url, headers, isFile) {
    var requestObj = {
        url: url,
        method: 'GET',
    }
    if(isFile) {
        requestObj['encoding'] =  null;
    }
    if(headers) {
        requestObj['headers'] = headers;
    }
    return new Promise((resolve, reject) => {
        console.log('headers = ', headers);
        // resolve(postData);
        request(requestObj, 
            function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    // var bodyObj = JSON.parse(body);
                    // console.log('bodyObj = ', bodyObj);
                    resolve(body);
                }
            }
        );
    });
}

function getUpdatedTime(airlineName, departureId, arrivalId) {
    var DBObject = LccSeat;
    return new Promise((resolve, reject) => {
        DBObject.findOne({airlineName:airlineName, departureId:departureId, arrivalId:arrivalId}, (err, data) =>{
            if(err) {
                resolve(undefined);
            } else {
                if(data) {
                    resolve(data.updated);
                }
                resolve(undefined);
            }
        });
    });
}

async function crawlJinAir() {
    if(!dbConnectCompleted) {
        await myTimeout(3000).then(() => {
            console.log('setTimeout completed');
        });
    }
    while(true) {
        console.log('crawlJin Loop 시작');
        for(let departCode in FLIGHT_INFO_JINAIR) {
            // if(departCode != 'PUS') continue; //테스트로 부산만
            let fromCity = getCityInfo(departCode, AIRPORT_INFO_JINAIR);
            for(var areaKey in FLIGHT_INFO_JINAIR[departCode]) {
                console.log('areaKey = ', areaKey);
                await crawlJinArea(areaKey, fromCity);
            }
        }
        await myTimeout(10*60*1000);
    }
}

async function crawlJinArea(toArea, fromCity) {
    var flightInfos = FLIGHT_INFO_JINAIR[fromCity.airport][toArea];
    for(let i = 0; i < flightInfos.length; i++) {
        var diffUpdatedHour = 100;
        var dateChanged = true;
        let cityCode = flightInfos[i].airport;
        // if(cityCode != 'GUM') continue;
        let updatedTime = await getUpdatedTime('JINAIR', fromCity.airport, flightInfos[i].airport);
        console.log('updatedTime = ', updatedTime);
        var curDate = new Date();
        if(updatedTime) {
            var diffUpdated = curDate - updatedTime;
            var hourUnit = 1000*60*60;
            diffUpdatedHour = parseInt(diffUpdated/hourUnit);
            dateChanged = (curDate.getDate() != updatedTime.getDate());
            console.log('diffUpdatedHour = %i, curD = %i, updateD = %i dateChanged = ' + dateChanged, diffUpdatedHour, curDate.getDate(), updatedTime.getDate());
        } else {
            console.log('diffUpdatedHour = %i, curD = %i', diffUpdatedHour, curDate.getDate());
        }
        if(diffUpdatedHour >= 23 || dateChanged) {
            let result = false;
            let retryCount = 10;
            let curEnvironment;
            let toCity = getCityInfo(cityCode, AIRPORT_INFO_KOREAN);
            while(!result && retryCount-- > 0) {
                console.log('진에어 ' + fromCity.airport + ' > ' + toCity.airport + ' 탐색 시작!!!');
                let resultObj = await crawlJinCity(toArea, toCity, curEnvironment, fromCity);
                result = resultObj.result;
                curEnvironment = resultObj.environment;
            }
        } else {
            console.log('진에어 ' + fromCity.airport + ' > ' + cityCode + ' 업데이트 존재!!!');
        }
    }
}


async function crawlJinCity(toArea, toCityInfo, prevEnvironment, fromCityInfo) {
    var dbWriteCompleted = false;
    var flightInfoObj = undefined;
    var environment = {};
    if (prevEnvironment) {
        environment = prevEnvironment;
    } else {
        environment['availableSeat'] = [{}, {}];
        environment['notAvailableSeat'] = [{}, {}];
        environment['remainSeat'] = [{}, {}];
        environment['curDepartureDate'] = new Date();
        environment['curArrivalDate'] = new Date();

        environment['curDepartureDate'].setMonth(9, 24);
        environment['curArrivalDate'].setMonth(9, 24);

        environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
        // environment['funcYYYYMMDD'] = dateToYYYYMMDD;
        // environment['funcYYYYMMDDWith'] = dateToYYYYMMDDWith;
        environment['curDepartureDateStr'] = dateToYYYYMMDD(environment['curDepartureDate']);
        environment['curArrivalDateStr'] = dateToYYYYMMDD(environment['curArrivalDate']);
        environment['departureAreaR'] = 'KR';
        if(fromCityInfo) {
            environment['departureAirportR'] = fromCityInfo.airport;
            environment['departureCityNameR'] = fromCityInfo.cityName;
            environment['departureCityR'] = fromCityInfo.cityName;
        } else {
            environment['departureAirportR'] = 'ICN';
            environment['departureCityNameR'] = '서울';
            environment['departureCityR'] = '서울';
        }
        environment['arrivalAirportR'] = toCityInfo.airport;
        environment['arrivalCityR'] = toCityInfo.city;
        environment['arrivalAreaR'] = toArea;
        environment['arrivalCityNameR'] = toCityInfo.cityName;
    
        environment['finalSearchDate'] = new Date();
        environment['finalSearchDate'].setDate(environment['finalSearchDate'].getDate()+360);
        environment['finalSearchDateStr'] = dateToYYYYMMDD(environment['finalSearchDate']);
    
        console.log('finalSearchDateStr = ', environment['finalSearchDateStr']);
    }

    // const browser = await puppeteer.launch({args : [
    const browser = await puppeteer.launch({headless: false, args : [
        '--window-size=1280,960'
    ]});
    console.log('curDepartureDate = ', environment['curDepartureDateStr']);
    console.log('curArrivalDate = ', environment['curArrivalDateStr']);
    const page = await browser.newPage();

    page.on('response', async response => {
        const req = response.request();
        if(req.url().indexOf('booking/getAirAvailabilityJson') > 0) {
            console.log('-------' + req.url() + '---------');
            let  availJson = await response.text();
            // console.log('availJson = ' + availJson);
            try{
                flightInfoObj = JSON.parse(availJson);
            } catch(err) {
                flightInfoObj = {parse_error:true};
                console.log('availJson = ' + availJson);
            }
            // console.log('flightInfoObj = ', util.inspect(flightInfoObj, false, null, true));
        }
      });

    page.on('console', msg => {
        for (let i = 0; i < msg.args.length; ++i)
            console.log(`${i}: ${msg.args[i]}`);
        });
    page.on('dialog', async dialog => {
        // console.log(dialog.message());
        await dialog.dismiss();
    });
    await page.setViewport({
        width: 1280,
        height: 960
    })
    try {
        const confirmButton = '#btnConfirm';
        const currencyButton = 'div.typeSel > button.btnTypeB';
        await page.goto('https://www.jinair.com/booking/index');
        await page.waitFor(1000);
        await page.click(confirmButton);
        await page.waitFor(3000);
        // let headHTML = await page.evaluate(() => document.head.innerHTML);
        // console.log('page.html = ', headHTML);
        // let csrf = undefined;
        // let maxLoopCount = 1000;
        // let searchStartIndex = 0;
        // while(csrf == undefined && maxLoopCount-- > 0) {
        //     searchStartIndex = headHTML.indexOf('<script', searchStartIndex + 1);
        //     if(searchStartIndex < 0) break;
        //     let srcStartIndex = headHTML.indexOf('src="', searchStartIndex + 1) + 5;
        //     let srcEndIndex = headHTML.indexOf('"', srcStartIndex+1);
        //     let srcAddress = headHTML.substring(srcStartIndex, srcEndIndex);
        //     if(srcAddress.indexOf("X-CSRF-TOKEN") >= 0) {
        //         srcAddress = srcAddress.replace("&amp;", "&");
        //         const myURL = new URL("http://test.com" + decodeURIComponent(srcAddress));
        //         console.log(myURL.searchParams.get('key'));
        //         console.log(myURL.searchParams.get('val'));
        //         csrf = myURL.searchParams.get('val');
        //     }
        //     console.log('srcAddress = ', srcAddress);
        // }
        await page.evaluate((environment) => {
            let registerForm = document.querySelector('#registerform');
            registerForm.querySelector('#adultPaxCount').value = '1';
            registerForm.querySelector('#origin1').value = environment['departureAirportR'];
            registerForm.querySelector('#destination1').value = environment['arrivalAirportR'];
            registerForm.querySelector('#travelDate1').value = environment['curDepartureDateStr'];
            registerForm.querySelector('#origin2').value = environment['arrivalAirportR'];
            registerForm.querySelector('#destination2').value = environment['departureAirportR'];
            registerForm.querySelector('#travelDate2').value = environment['curArrivalDateStr'];
            registerForm.querySelector('#tripType').value = 'RT';
            registerForm.querySelector('#domIntType').value = 'INT';

            registerForm.submit();
        }, environment);

        await page.waitFor(3000);
        // await myTimeout(500000);

        environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
        environment['curArrivalDateStr'] = dateToYYYYMMDD(environment['curArrivalDate']);

        let loopCount = 1000;
        // while(loopCount-- > 0) {
        //     let result = await page.evaluate((environment) => {
        //         _cf['_setFsp'] = true;

        //         var _cf = _cf || []; _cf.push(['_setFsp', true]); _cf.push(['_setBm', true]); _cf.push(['_setAu', '/public/59a3d56ad195eb5a3ddb7993036cf']);
        //         return _cf;
        //     });
        //     console.log('result = ', result);
        //     await myTimeout(100);
        // }
        let disabledCount = 0;
        let currencyClickCount = 0;
        let departureEnd = false;
        let arrivalEnd = false;

        while(loopCount-- > 0) {
            if(currencyClickCount % 2 == 1) {
                console.log('currencyButton!!!');
                await page.click(currencyButton);
            }
            currencyClickCount++;

            let randomVal = getRandomInt(1, 7);
            randomVal = 2;
            // console.log('randomVal = ', randomVal);
            await page.waitFor(randomVal*1000);
            flightInfoObj = undefined;
            await page.evaluate((environment) => {
                let registerForm = document.querySelector('#registerform');
                registerForm.querySelector('#travelDate1').value = environment['curDepartureDateStr'];
                registerForm.querySelector('#travelDate2').value = environment['curArrivalDateStr'];
            }, environment);


            // var arrivalDate = environment['curArrivalDateStr'].substring(0, 4) + '-' + environment['curArrivalDateStr'].substring(4, 6) + '-' + environment['curArrivalDateStr'].substring(6, 8);
            // // console.log('arrivalDate = ', arrivalDate);
            // let result = await page.evaluate((environment) => {
            //     // console.log('_cf = ', _cf);
            //     // console.log('_cf = ', _cf);
            //     let originDestinationInfos = document.querySelectorAll('.originDestinationInfo');
            //     originDestinationInfos.forEach((originDestinationInfo) => {
            //         let indexno = originDestinationInfo.getAttribute('indexno');
            //         let origin = originDestinationInfo.getAttribute('origin');
            //         let destination = originDestinationInfo.getAttribute('destination');
            //         if(indexno == '1') {
            //             let lis = originDestinationInfo.querySelectorAll('li');
            //             lis.forEach((li) => {
            //                 let flightdate = li.getAttribute('flightdate');
            //                 let arrivalDate = environment['curArrivalDateStr'].substring(0, 4) + '-' + environment['curArrivalDateStr'].substring(4, 6) + '-' + environment['curArrivalDateStr'].substring(6, 8);   
            //                 if(flightdate == arrivalDate) {
            //                     let ahref = li.querySelector('a');
            //                     // ahref.click();
            //                     ahref.setAttribute('class', 'myclick-target');
            //                 }
            //             });
            //         }
            //     });
            // }, environment);
            // await page.click('.myclick-target');

            await page.evaluateHandle(() => {
                // fnClickBestFare(null, 1, arrivalDate, '')
                getNormalBestFare(0);
            });
            let waitCount = 30;
            while(flightInfoObj == undefined && waitCount-- > 0) {
                await myTimeout(500);
            }
            if(flightInfoObj['parse_error'] == true) {
                throw 'parse_error';
            }
            let pushCount = 0;
            console.log('flightInfoObj = ', flightInfoObj);
            if(flightInfoObj["errorCode"] == null) {
                for(let i = 0; i < flightInfoObj.result.originDestinationInfo.length; i++) {
                    let originDestinationInfo = flightInfoObj.result.originDestinationInfo[i];
                    let curDate;
                    let typeIdx = i;
                    if(typeIdx == 0) {
                        curDate = environment['curDepartureDateStr'];
                    } else {
                        curDate = environment['curArrivalDateStr'];
                    }
                    for(let j = 0; j < originDestinationInfo.tripInfo.length; j++) {
                        let segmentInfos = originDestinationInfo.tripInfo[j].segmentInfo;
                        for(let k = 0; k < segmentInfos.length; k++) {
                            let segmentInfo = segmentInfos[k];
                            let scheldule = segmentInfo.departureTime + '-' + segmentInfo.arrivalTime;
                            if(!environment['remainSeat'][typeIdx][curDate]) {
                                environment['remainSeat'][typeIdx][curDate] = {};
                            }
                            if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
                                environment['remainSeat'][typeIdx][curDate][scheldule] = {
                                    aircraftType : segmentInfo.aircraft,
                                    sdate : curDate,
                                    stime : scheldule,
                                    flightTime: segmentInfo.journeyTimeText,
                                    seatData : []
                                };
                            }                        
                            for(let segIdx = 0; segIdx < segmentInfo.segmentAvailability.length; segIdx++) {
                                let segmentAvailability = segmentInfo.segmentAvailability[segIdx];
                                let seatCount = segmentAvailability.seatAvailablity;
                                if(segmentAvailability.seatAvailablity > 0) {
                                    pushCount++;
                                    environment['remainSeat'][typeIdx][curDate][scheldule]['seatData'].push({
                                        seatType: segmentAvailability.fareTypeNm,
                                        seatNum: seatCount,
                                        fare: segmentAvailability.appliedFareAmount,
                                        tax: segmentAvailability.taxAmount,
                                        totalFare: segmentAvailability.appliedFareAmount + segmentAvailability.taxAmount,
                                        condition: {}
                                    });
                                }
                            }
                        }
                    }
                    for(let j = 0; j < originDestinationInfo.bestFareInfo.length; j++) {
                        let bestFareInfo = originDestinationInfo.bestFareInfo[j];
                        if(bestFareInfo.bestFare < 0) {
                            let flightDate = bestFareInfo.flightDate.replace(/-/gi, '');
                            if(flightDate > curDate) {
                                environment['notAvailableSeat'][typeIdx][flightDate] = bestFareInfo.bestFare;
                            }
                        }
                    }
                    console.log('depart notAvailableSeat = ', environment['notAvailableSeat'][0]);
                    console.log('arrive notAvailableSeat = ', environment['notAvailableSeat'][1]);
                }
            }
            if(pushCount == 0) {
                disabledCount++;
            }
            if(disabledCount > 5) {
                console.log(`break searching[${environment['curDepartureDate']}-${environment['curArrivalDate']}]`);
                break;
            }

            environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
            let searchCount = 30;
            while(--searchCount > 0) {
                let dateWith = dateToYYYYMMDD(environment['curArrivalDate']);
                if(environment['notAvailableSeat'][1][dateWith] === undefined) break;
                if(environment['notAvailableSeat'][1][dateWith] === -1)  {
                    arrivalEnd = true;
                    break;
                }
                // console.log(dateWith + ' arrival ' + environment['notAvailableSeat'][1][dateWith]);
                environment['curArrivalDate'].setDate(environment['curArrivalDate'].getDate()+1);
            }
            environment['curArrivalDateStr'] = dateToYYYYMMDD(environment['curArrivalDate']);

            if(environment['notAvailableSeat'][0][environment['curDepartureDate']] !== -1) {
                environment['curDepartureDate'].setDate(environment['curDepartureDate'].getDate()+1);
                searchCount = 30;
                while(--searchCount > 0) {
                    let dateWith = dateToYYYYMMDD(environment['curDepartureDate']);
                    if(environment['notAvailableSeat'][0][dateWith] === undefined) break;
                    if(environment['notAvailableSeat'][0][dateWith] === -1) {
                        departureEnd = true;
                        break;
                    }
                    // console.log(dateWith + ' departure ' + environment['notAvailableSeat'][0][dateWith]);
                    if(dateWith == environment['curArrivalDateStr']) break;
                    environment['curDepartureDate'].setDate(environment['curDepartureDate'].getDate()+1);
                }
                environment['curDepartureDateStr'] = dateToYYYYMMDD(environment['curDepartureDate']);        
            }

            console.log('depart ' + environment['curDepartureDateStr'] + ' arrive ' + environment['curArrivalDateStr']);


            if(arrivalEnd && departureEnd) {
                break;
            }

            if(environment['curDepartureDate'].getTime() > environment['finalSearchDate'].getTime() && environment['curArrivalDate'].getTime() > environment['finalSearchDate'].getTime()) {
                break;
            }
        }
        console.log('environment = ', util.inspect(environment, false, null, true));

        // await page.evaluate((environment) => {
        //     let reisterForm = document.querySelector('#registerform');
        //     reisterForm.submit();
        // }, environment);

        // let mobileAppIndex = headHTML.indexOf('mobileApp.js?');
        // let keyStartIndex = headHTML.indexOf('key=', mobileAppIndex) + 4;
        // let valStartIndex = headHTML.indexOf('val=', mobileAppIndex) + 4;
        // let keyEndIndex = headHTML.indexOf('', mobileAppIndex) + 4;
        // let valEndIndex = headHTML.indexOf('val=', mobileAppIndex) + 4;
        // let csrf = await page.$eval("head > meta[name='_csrf']", element => element.textContent);
        // let csrfHeader = await page.$eval("head > meta[name='_csrf_header']", element => element.textContent);
        // console.log('csrf = ', csrf);
        // console.log('csrfHeader = ', csrfHeader);

        // await page.waitFor(30*60*1000);
        saveLCCSeat('JINAIR', environment['departureAirportR'], environment['departureCityNameR'], environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['remainSeat'][0]);
        saveLCCSeat('JINAIR', environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['departureAirportR'], environment['departureCityNameR'], environment['remainSeat'][1]);
        dbWriteCompleted = true;
        // await page.waitForNavigation();
        // await page.screenshot({path: 'example.png'});
    } catch(err) {
        console.log('Exception catch!!!!!');
        console.log(err);
    }
    await browser.close();
    return {result: dbWriteCompleted, environment:environment};
}

async function crawlJejuAir() {
    if(!dbConnectCompleted) {
        await myTimeout(3000).then(() => {
            console.log('setTimeout completed');
        });
    }
    while(true) {
        console.log('crawlJeju Loop 시작');
        for(let departCode in FLIGHT_INFO_KOREAN) {
            // if(departCode != 'PUS') continue; //테스트로 부산만
            let fromCity = getCityInfo(departCode, AIRPORT_INFO_KOREAN);
            for(var areaKey in FLIGHT_INFO_KOREAN[departCode]) {
                console.log('areaKey = ', areaKey);
                await crawlJejuArea(areaKey, fromCity);
            }
        }
        await myTimeout(10*60*1000);
    }
}

async function crawlJejuArea(toArea, fromCity) {
    var flightInfos = FLIGHT_INFO_KOREAN[fromCity.airport][toArea];
    for(let i = 0; i < flightInfos.length; i++) {
        var diffUpdatedHour = 100;
        var dateChanged = true;
        let cityCode = flightInfos[i].airport;
        if(cityCode != 'GUM') continue;
        // let updatedTime = await getUpdatedTime('KOREAN', fromCity.airport, flightInfos[i].airport);
        let updatedTime = false;
        console.log('updatedTime = ', updatedTime);
        var curDate = new Date();
        if(updatedTime) {
            var diffUpdated = curDate - updatedTime;
            var hourUnit = 1000*60*60;
            diffUpdatedHour = parseInt(diffUpdated/hourUnit);
            dateChanged = (curDate.getDate() != updatedTime.getDate());
            console.log('diffUpdatedHour = %i, curD = %i, updateD = %i dateChanged = ' + dateChanged, diffUpdatedHour, curDate.getDate(), updatedTime.getDate());
        } else {
            console.log('diffUpdatedHour = %i, curD = %i', diffUpdatedHour, curDate.getDate());
        }
        if(diffUpdatedHour >= 23 || dateChanged) {
            let result = false;
            let retryCount = 3;
            let curEnvironment;
            let toCity = getCityInfo(cityCode, AIRPORT_INFO_KOREAN);
            while(!result && retryCount-- > 0) {
                console.log('제주항공 ' + fromCity.airport + ' > ' + toCity.airport + ' 탐색 시작!!!');
                let resultObj = await crawlJejuCity(toArea, toCity, curEnvironment, fromCity);
                result = resultObj.result;
                curEnvironment = resultObj.environment;
            }
        } else {
            console.log('제주항공 ' + fromCity.airport + ' > ' + cityCode + ' 업데이트 존재!!!');
        }
    }
}

async function crawlJejuCity(toArea, toCityInfo, prevEnvironment, fromCityInfo) {    
    var dbWriteCompleted = false;
    var environment = {};
    if (prevEnvironment) {
        environment = prevEnvironment;
    } else {
        environment['availableSeatCompleted'] = false;
        environment['availableSeat'] = [{}, {}];
        environment['remainSeat'] = [{}, {}];
        environment['searchStartDate'] = new Date();
        environment['searchStartDateStr'] = dateToYYYYMMDDWith(environment['searchStartDate']);
        environment['curSearchDate'] = new Date();
        environment['curSearchDate'].setTime(environment['searchStartDate'].getTime());
        environment['curSearchDateStr'] = dateToYYYYMMDDWith(environment['curSearchDate']);
        environment['departureAreaR'] = 'KR';
        if(fromCityInfo) {
            environment['departureAirportR'] = fromCityInfo.airport;
            environment['departureCityNameR'] = fromCityInfo.cityName;
            environment['departureCityR'] = fromCityInfo.cityName;
        } else {
            environment['departureAirportR'] = 'ICN';
            environment['departureCityNameR'] = '서울';
            environment['departureCityR'] = '서울';
        }
        environment['arrivalAirportR'] = toCityInfo.airport;
        environment['arrivalCityR'] = toCityInfo.city;
        environment['arrivalAreaR'] = toArea;
        environment['arrivalCityNameR'] = toCityInfo.cityName;

        environment['typeIdx'] = 0;
    
        environment['finalSearchDate'] = new Date();
        environment['finalSearchDate'].setDate(environment['finalSearchDate'].getDate()+30);
        environment['finalSearchDateStr'] = dateToYYYYMMDDWith(environment['finalSearchDate']);
    
        console.log('finalSearchDateStr = ', environment['finalSearchDateStr']);
    }

    try {
        for(let typeIdx = 0; typeIdx < 2; typeIdx++) {
        // for(let typeIdx = 0; typeIdx < 1; typeIdx++) {
            environment['typeIdx'] = typeIdx;
            if(typeIdx == 0) {
                departureCode = environment['departureAirportR'];
                arrivalCode = environment['arrivalAirportR'];    
            } else {
                arrivalCode = environment['departureAirportR'];
                departureCode = environment['arrivalAirportR'];    
            }
            let maxLoopCount = 1000;
            while(environment['curSearchDateStr'] < environment['finalSearchDateStr'] && --maxLoopCount > 0) {
                let dateKey = environment['curSearchDateStr'];
                console.log('dateKey = ', dateKey);
                let flightRes = await requestUrlPostObj('https://ibsearch.jejuair.net/jejuair/com/jeju/ibe/availHybris.do', '{"AdultPaxCnt": 1, "ChildPaxCnt": 0, "InfantPaxCnt": 0, "RouteType": "I", "Language": "KR", "ReturnSeatAvail": true, "PointsPayment": false, "TripType": "RT", "DepDate": "' + dateKey + '", "SegType": "DEP", "DepStn": "' + departureCode + '", "ArrStn": "' + arrivalCode + '"}' );

                // console.log('flightRes = ', flightRes);
                for(let i = 0; i < flightRes.Result.Data.AvailabilityDates.length; i++) {
                    let AvailabilityDates = flightRes.Result.Data.AvailabilityDates[i];
                    let fDate = AvailabilityDates.Date;
                    let curDate = fDate.replace(/-/gi, '');
                    if(!environment['remainSeat'][typeIdx][curDate]) {
                        environment['remainSeat'][typeIdx][curDate] = {};
                    }
                    // console.log('AvailabilityDates = ', AvailabilityDates);
                    if(AvailabilityDates.Availability) {
                        for(let j = 0; j < AvailabilityDates.Availability.length; j++) {
                            let Availability = AvailabilityDates.Availability[j];
                            let fares = Availability.Fares;
                            let segment = Availability.FlightSegment;
                            let fTime = segment.DepTime;
                            let departure = segment.DepStn;
                            let arrival = segment.ArrStn;
                            let scheldule = segment.DepTime + '-' + segment.ArrTime;
                            if(!environment['remainSeat'][typeIdx][curDate][scheldule]) {
                                environment['remainSeat'][typeIdx][curDate][scheldule] = {
                                    aircraftType : segment.AircraftType,
                                    sdate : curDate,
                                    stime : scheldule,
                                    seatData : []
                                };
                            }
                            for(let k = 0; k < fares.length; k++) {
                                let fType = fares[k].Name.split(' ')[0];
                                console.log(`[${departure} > ${arrival}] ${fDate} ${fTime} ${fType} ${fares[k].TotalFare} - ${fares[k].SeatCount}석`);
                                environment['remainSeat'][typeIdx][curDate][scheldule]['seatData'].push({
                                    seatType: fType,
                                    seatNum: fares[k].SeatCount,
                                    fare: fares[k].TotalFare,
                                    tax: 0,
                                    totalFare: fares[k].TotalFare,
                                    condition: fares[k].FareRule.Regulation
                                });
                            }
                        }
                    }
                }
                
                await myTimeout(200);
                environment['curSearchDate'].setDate(environment['curSearchDate'].getDate()+7);
                environment['curSearchDateStr'] = dateToYYYYMMDDWith(environment['curSearchDate']);
            }
            //출발날짜 초기화
            environment['curSearchDate'].setTime(environment['searchStartDate'].getTime());
            environment['curSearchDateStr'] = environment['searchStartDateStr'];
        }

        //확인 로그
        // let typeIdx = 0;
        // let depCode, arrCode;
        // if(typeIdx == 0) {
        //     depCode = environment['departureAirportR'];
        //     arrCode = environment['arrivalAirportR'];    
        // } else {
        //     depCode = environment['arrivalAirportR'];    
        //     arrCode = environment['departureAirportR'];
        // }
        // for(let sdate in environment['remainSeat'][typeIdx]) {
        //     for(let stime in environment['remainSeat'][typeIdx][sdate]) {
        //         let remainObj = environment['remainSeat'][typeIdx][sdate][stime];
        //         console.log('%s -> %s (%s %s) : ', depCode, arrCode, sdate, stime, remainObj);
        //     }
        // }
        
        saveLCCSeat('JEJUAIR', environment['departureAirportR'], environment['departureCityNameR'], environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['remainSeat'][0]);
        saveLCCSeat('JEJUAIR', environment['arrivalAirportR'], environment['arrivalCityNameR'], environment['departureAirportR'], environment['departureCityNameR'], environment['remainSeat'][1]);
        dbWriteCompleted = true;
    } catch(err) {
        console.log('Exception catch!!!!!');
        console.log('err : ', err);
    }
    // await browser.close();
    return {result: dbWriteCompleted, environment:environment};
}

function saveLCCSeat(airlineName, departureId, departureName, arrivalId, arrivalName, seatData) {
    console.log('DB WRITE %s(%s) -> %s(%s)', departureName, departureId, arrivalName, arrivalId);
    // console.log("seatData = ", seatData);
    let seat = new LccSeat();
    seat.airlineName = airlineName;
    seat.departureId = departureId;
    seat.departureName = departureName;
    seat.arrivalId = arrivalId;
    seat.arrivalName = arrivalName;
    for (let sdate in seatData) {
        for (let stime in seatData[sdate]) {
            // let flightTimeMinute = parseInt(seatData[sdate][stime]['flightTime']/60);
            // let flightTime = parseInt(flightTimeMinute/60) + '시간 ' + (flightTimeMinute%60) + '분';
            seat.seatState.push(seatData[sdate][stime]);
            // seat.seatState.push({sdate:sdate, stime:stime, aircraftType:seatData[sdate][stime]['aircraftType'], seatNum:seatData[sdate][stime]['seatNum']});
        }
    }
    LccSeat.findOneAndUpdate(
        {
            "airlineName" : seat.airlineName,
            "departureId" : seat.departureId,
            "arrivalId" : seat.arrivalId
        },
        {
            $currentDate: {
                updated: true
            },
            $set : {
                "departureName" : seat.departureName,
                "arrivalName" : seat.arrivalName,
                "seatState" : seat.seatState
            }
        },
        {
           upsert : true,
           new:true
        },
        (err, newSeat) => {
            console.log('err - ', err);
            // console.log('newSeat - ', newSeat);
        }
    );
}