var winston = require("winston");
require('winston-daily-rotate-file');

var config = require("./config");
var fs = require("fs");

var logDir = config.LOG_ROOT_PATH;

if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => new Date().toLocaleTimeString();

var logger = new winston.createLogger({
    transports: [
        new winston.transports.Console({
            timestamp: tsFormat,
            colorize: true,
            level: "debug"
        }),
        new winston.transports.DailyRotateFile({
            name: 'File#debug',
            level: "debug",
            filename: `${logDir}/debug-logs.log`,
            timestamp: tsFormat,
            datePattern: "yyyy-MM-dd",
            prepend: true,
            json:false
        }),
        new winston.transports.DailyRotateFile({
            name: 'File#log',
            level: "info",
            filename: `${logDir}/info-logs.log`,
            timestamp: tsFormat,
            datePattern: "yyyy-MM-dd",
            prepend: true,
            json:false
        }),
        new winston.transports.DailyRotateFile({
            name: 'File#error',
            level: "error",
            filename: `${logDir}/error-logs.log`,
            timestamp: tsFormat,
            datePattern: "yyyy-MM-dd",
            prepend: true,
            json:false
        })
    ]
});

function debug() {
    logger.debug.apply(logger, arguments);
}

function log() {
    logger.info.apply(logger, arguments);
}

function error() {
    // var stack = new Error().stack;
    // Array.prototype.push.call(arguments, "\n\n" + stack);
    logger.error.apply(logger, arguments);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function dateToYYYYMMDD(date) {
    let YYYY = date.getFullYear();
    let month = date.getMonth() + 1;
    let MM = (month < 10?'0':'') + month;
    let dayOfMonth = date.getDate();
    let DD = (dayOfMonth < 10?'0':'') + dayOfMonth;
    return YYYY + MM + DD;
}
function dateToYYYYMMDDWith(date) {
    let YYYY = date.getFullYear();
    let month = date.getMonth() + 1;
    let MM = (month < 10?'0':'') + month;
    let dayOfMonth = date.getDate();
    let DD = (dayOfMonth < 10?'0':'') + dayOfMonth;
    return YYYY + '-' + MM + '-' + DD;
}

//YYYY-MM-DD -> MM-DD-YYYY
function convert2DateMMDDYYYY(dateStr) {
    return dateStr.substring(5, 7) + '-' + dateStr.substring(8, 10) + '-' + dateStr.substring(0, 4);
}

function myTimeout(sec) {
    // console.log('0.setTimeout completed');
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            // console.log('1.setTimeout completed');
            resolve();
        }, sec);
    });
}


module.exports = {
    uid: len => {
        let buf = [],
            chars =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
            charlen = chars.length;

        for (let i = 0; i < len; ++i) {
            buf.push(chars[getRandomInt(0, charlen - 1)]);
        }

        return buf.join("");
    },
    debug: debug,
    log: log,
    error: error,
    dateToYYYYMMDD: dateToYYYYMMDD,
    dateToYYYYMMDDWith: dateToYYYYMMDDWith,
    convert2DateMMDDYYYY: convert2DateMMDDYYYY,
    getRandomInt: getRandomInt,
    myTimeout: myTimeout
};
