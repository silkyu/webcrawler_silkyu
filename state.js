module.exports = {
    OPEN_STATUS : {
        testApp: "testApp",
        shopInfo: "shopInfo",
        pgRegister: "pgRegister",
        pgInfo: "pgInfo",
        pgFinish: "pgFinish",
        live: "live"
    }, 
    MEMBER_APPROVE_TYPE : {
        none: 0,
        must: 1
    }    
}