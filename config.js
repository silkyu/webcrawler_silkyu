var database ="pottrip";

module.exports = {
    secret: "weneepl_secret",
    LOG_ROOT_PATH: "./log",
    prod: false,
    indexing: false,
    mongodbUri: () => {
        return (
            "mongodb://weneeplapp:weneepl1@weneepl-shard-00-00-4bh3g.mongodb.net:27017,weneepl-shard-00-01-4bh3g.mongodb.net:27017,weneepl-shard-00-02-4bh3g.mongodb.net:27017/" +
            database +
            "?ssl=true&replicaSet=weneepl-shard-0&authSource=admin"
        );
    }
};
